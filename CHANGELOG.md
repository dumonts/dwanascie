# DEPRECATED

From v0.25.2 on, the changelog is represented by descriptive merge requests.


# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.25.1 - 2021-01-13

### Changed
* core: When a trace is created by with-span, two messages for begin and finish of this span are written to log (when log level is trace)


## 0.25.0 - 2020-03-26

### Added
* core: read secrets from path to support mounting secrets to directories in kubernetes
* core: add http readiness health check to metrics
* jdbc: add 'jdbc-username' and 'jdbc-password' to config
  

## 0.24.1 - 2019-10-10

# Fixed
* core: use all default values in nested config


## 0.24.0 - 2019-08-28

### Added
* core: improved cli handling with custom commands


## 0.23.0 - 2019-08-26

### Added
* server: new web server component

### Fixed
* amqp: clean shutdown
* api: clean shutdown
* jdbc: clean shutdown
* datomic: clean shutdown


## 0.22.1 - 2019-08-23

### Fixed
* core: readded org.flatland/ordered dep for java 11 compilation


## 0.22.0 - 2019-08-23

### Added
* core: read secrets from separate config file and merge with config
* core: add service-info map to each component


## 0.21.0 - 2019-06-21

### Added
* api: optional error data


## 0.20.1 - 2019-06-14

### Fixed
* api: client dispatch blocking


## 0.20.0 - 2019-06-12

### Fixed
* api: rpc call


## 0.19.0 - 2019-06-12

### Changed
* use cached thread pool for amqp connections.
* use amqp x-max-priority queue argument as default.
* api: add various client/server metrics
* api: additional tracing spans 


## 0.18.0 - 2019-05-17

### Added
* core: add utility to read version from project.clj


## 0.17.0 - 2019-05-14

### Fixed
* api: every consumer has own channel


## v0.16.0 - 2019-05-09

### Added
* core: edn <-> json conversion with schema based coersion
* api: server to client publish


## v0.15.0 - 2019-04-10

### Changed
* jaeger configuration follows https://github.com/jaegertracing/jaeger-client-java/blob/v0.33.1/jaeger-core/README.md


## v0.14.0 - 2019-04-05

### Added
* cli option to log active config on service start
* error/type and error/message for exception on rpc timeout
* get service/version from file resource/VERSION by default 

### Removed
* remove config option to disable metrics


## v0.13.0 - 2019-04-02

### Changed
* simplified system defintions
* new config system with yaml format
* metrics included by default
* auth based on keycloak

### Added
* rpc api


## v0.12.0 - 2019-02-13

### Added
* OpenTracing/Jaeger support in core, jdbc and amqp.


## v0.11.3 - 2019-02-05

### Fixed
* Catch InteruptException in amqp requester.

### Added
* Log using JSON format.


## v0.11.2 - 2019-01-10

### Fixed
* Log exception if system component fails to start.


## v0.11.1 - 2019-01-08

### Fixed
* Exit if system component fails to start.

## v0.11.0 - 2018-12-12

### Added
* Check that JVM was started with file.encoding set to UTF-8

## v0.10.0 - 2018-09-18

### Added
* Dynamic systems that can change system defintions based on runtime config.


## v0.9.0 - 2018-07-12

### Added
* content-type application/json for rabbitmq messages.

### Fixed
* Default exception handler return log fn instead of executing log fn.


## v0.8.0 - 2017-12-12

### Added
* Multiple attempts to establish rabbitmq connection.


## v0.7.0 - 2017-12-11

### Added
* Responder filter mode.
* Consumer pull mode.


## v0.6.0 - 2017-09-19

### Added
* Start multiple responder channels/consumers based on :consumer-count config setting.
* Prometheus metrics exporter component.


## v0.5.1 - 2017-08-19

### Added
* Both :disabled? and :disabled config options disable components.


## v0.5.0 - 2017-08-11

### Added
* optional OTP authentication.
* max login attempts handling.


## v0.4.0 - 2017-07-03

### Added
* Set content-type and content-encoding on AMQP messages.
* Configure connection-name on AMQP connections.
* Configure qos prefetch count on AMQP consumers.
* Update com.novemberain/langohr to 4.0.0.


## v0.3.0 - 2017-06-30

### Added
* JDBC component.
* User authentication/authorization component.
* Provide service info via heartbeat fn.


## v0.2.0 - 2017-06-13

### Added
* A system is a node that exposes services.
* Node with services heartbeats (without health checks).
* Monitor available nodes/services.


## v0.1.0 - 2017-05-16

### Added
* read config from environment vars, properties or EDN file using omniconf.
* logging to console via logback using unilog.
* runtime lifecyle management using sierra components.
* simple cli interface using tools.cli.
* shutdown hook.
* log uncaught thread exceptions.
* datomic peer component.
* AMQP client components: connection, channel, producer, consumer, request/response.
