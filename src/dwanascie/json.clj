(ns dwanascie.json
  (:require [jsonista.core :as j]
            [schema.core :as s]
            [schema.coerce :as sc])

  (:import java.time.Instant
           java.util.Date
           java.util.regex.Pattern
           (schema.core CondPre
                        ConditionalSchema
                        Constrained
                        Maybe
                        One
                        OptionalKey
                        Predicate
                        RequiredKey)))


;; Schema to validate if a schema is json compatible

;; TODO nil? clojure.lang.Ratio Float/NaN Float/POSITIVE_INFINITY Float/NEGATIVE_INFINITY

(s/defschema AtomicSchema
  (s/enum s/Bool s/Int s/Num s/Str s/Keyword s/Uuid s/Symbol s/Regex s/Inst Instant))

(declare Compound)

(s/defschema VectorSchema
  [(s/if #(instance? One %)
     {:schema    (s/recursive #'Compound)
      :optional? s/Bool
      :name      s/Str}
     (s/recursive #'Compound))])

(s/defschema SetSchema
  #{(s/recursive #'Compound)})

(s/defschema KeySchema
  (s/if keyword?
    s/Keyword
    (s/enum s/Str s/Keyword s/Uuid s/Symbol s/Regex Instant)))

(s/defschema KeywordKeySchema
  (s/conditional
    keyword? s/Keyword
    #(or (instance? OptionalKey %)
         (instance? RequiredKey %)) {:k s/Keyword}))

(s/defschema MapSchema
  (s/if #(= 1 (count %))
    {KeySchema (s/recursive #'Compound)}
    {KeywordKeySchema (s/recursive #'Compound)}))

(s/defschema ConditionalCompound
  {:preds-and-schemas [[(s/one s/Any "pred") (s/one (s/recursive #'Compound) "schema")]]
   :error-symbol      s/Any})

(s/defschema CondPreCompound
  {:schemas [(s/recursive #'Compound)]})

(s/defschema ConstrainedCompound
  {:schema        (s/recursive #'Compound)
   :postcondition s/Any
   :post-name     s/Any})

(s/defschema MaybeCompound
  {:schema (s/recursive #'Compound)})

;; TODO cond-pre

(defn schema-map? [m]
  (and (map? m)
       (not (instance? Predicate m))))

(s/defschema Compound
  (s/conditional
    #(instance? ConditionalSchema %) ConditionalCompound
    #(instance? Constrained %) ConstrainedCompound
    #(instance? CondPre %) CondPreCompound
    #(instance? Maybe %) MaybeCompound
    schema-map? MapSchema
    vector? VectorSchema
    set? SetSchema
    :else AtomicSchema))

(s/defschema JSONCompatible
  (s/recursive #'Compound))


;; Converter

(defn keyword->string [k]
  (if (keyword? k)
    (if-let [nspace (namespace k)]
      (str nspace "/" (name k))
      (name k))
    k))

(defn string->symbol [s]
  (if (string? s) (symbol s) s))

(defn string->regex [s]
  (if (string? s) (Pattern/compile s) s))

(defn string->inst [s]
  (if (string? s) (-> s (Instant/parse) (Date/from)) s))

(defn string->time-inst [s]
  (if (string? s) (Instant/parse s) s))

;; TODO restrict usage of s/Num ??
(defn string->num [s]
  (if (string? s)
    (case s
      "Infinity" Double/POSITIVE_INFINITY
      "-Infinity" Double/NEGATIVE_INFINITY
      "NaN" Double/NaN
      s)
    s))

(defn prepare-str [k]
  (if (keyword? k)
    (subs (str k) 1)
    k))

(def coercions
  {s/Num    string->num
   s/Str    keyword->string
   s/Uuid   (comp sc/string->uuid prepare-str)
   s/Symbol (comp string->symbol prepare-str)
   s/Regex  (comp string->regex prepare-str)
   s/Inst   (comp string->inst prepare-str)
   Instant  (comp string->time-inst prepare-str)})

(defn- json-coercer [schema]
  (or (coercions schema)
      (sc/json-coercion-matcher schema)))

(defn converter [schema]
  (s/validate JSONCompatible schema)
  {:coercer (sc/coercer! schema json-coercer)
   :schema schema})

(def json-mapper (j/object-mapper {:date-format   "yyyy-MM-dd'T'HH:mm:ss.S'Z'"
                                   :decode-key-fn true}))

(defn edn->json [{:keys [schema] :as converter} edn]
  (-> (s/validate schema edn)
      (j/write-value-as-string json-mapper)))

(defn json->edn [{:keys [coercer schema] :as converter} s]
  (->> (j/read-value s json-mapper)
       (coercer)
       (s/validate schema)))