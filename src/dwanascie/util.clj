(ns dwanascie.util
  (:require [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.pprint :as pp]
            [clojure.stacktrace :as st]
            [clojure.tools.logging :as log]
            [com.stuartsierra.component :as component])
  (:import java.time.OffsetDateTime
           java.util.Properties))

(defn get-timestamp
  "Return current time instant in ISO-8601 representation with offset from UTC/Greenwich"
  []
  (str (OffsetDateTime/now)))

(defn project-version
  ([]
   (:version (edn/read-string (slurp (io/resource "version.edn")))))

  ([group artifact]
   (let [file       (-> "META-INF/maven/%s/%s/pom.properties"
                        (format group artifact)
                        (io/resource)
                        (io/reader))
         properties (doto (Properties.)
                      (.load file))]
     (.get properties "version"))))

(defn pprint-str
  "Stringifies an item with the clojure.pprint pretty-printer"
  [x]
  (with-out-str (pp/pprint x)))

(defn cause-trace->str
  "Stringifies an exception chain by their type and messages with separator"
  [exception]
  (with-out-str (st/print-cause-trace exception)))

;; Component that has only config data

(defrecord ConfigData [name config config-shape]
  component/Lifecycle
  (start [this]
    (log/infof "Started global Config server %s" name)
    this)

  (stop [this]
    (log/info "Stopped global Config server %s" name)
    this))

(defn new-config-data [name config-shape default-config]
  (map->ConfigData {:name         name
                    :config-shape config-shape
                    :config       default-config}))
