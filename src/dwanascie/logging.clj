(ns dwanascie.logging
  (:require [clojure.tools.logging :as log]
            [com.stuartsierra.component :as component]
            [schema.core :as s]
            [unilog.config :as unilog]
            [unilog.context :as log-context]))

(defmacro with-time
  "Executes the body and logs its execution time."
  [level task-name & body]
  `(let [start#  (System/currentTimeMillis)
         result# (do ~@body)
         stop#   (System/currentTimeMillis)]
     (log/logf ~level "%s - %s msecs" ~task-name (- stop# start#))
     result#))
(defmacro with-context
  "Execute body with a mapped-diagnostic-context like {\"item-id\" \"<id>\"}.

  Convenience macro wrapping `unilog.context/with-context`.
  Logs a warning when \"item-id\" is not set.

  Use together with `with-mdc`, whenever code is executed in a different thread (e.g. pmap, future, thread, go, etc.).
  MDC-bindings will then be sustained over spawned threads."
  [mdc & body]
  `(if (get ~mdc "item-id")
     (log-context/with-context ~mdc
       ~@body)
     (do (log/warn (IllegalArgumentException.)
           "dwanascie.logging/with-context expects a key-value-pair {\"item-id\" \"<id>\"} for proper logging")
       ~@body)))

(defn with-mdc
  "Logger context wrapper for MDCs (Mapped Diagnostic Context).
  Useful for `%mdc{item-id}` etc. in the Logger pattern.

  Execution in another thread (`pmap`, `future`, etc.)"
  [f]
  (log-context/mdc-fn* f))

(comment
 ;; use the unilog/with-context fn to fill mapped diagnostic context %mdc{item-id}
 ;; allows easy to use traceability of pipelined items:
 (require '[clojure.tools.logging :as log])
 (require '[unilog.context :refer [with-context]])

 (with-context {"item-id" (random-uuid)}
   (log/info "hello"))
 ;; => [2022-01-11 12:00:00,000] [main] INFO  dwanascie.logging:123 Item ABC123456789 - Processing...

 ;; Usage of with-mdc
 (with-context {"item-id" "Item 123"}
   (pmap (with-mdc (fn [x] (log/infof "consuming %s" x) 'do-stuff)) ['one 'two 'three]))
 ;; => INFO ... Item 123 - consuming one
 ;; => INFO ... Item 123 - consuming two
 ;; => INFO ... Item 123 - consuming three
 ,)

(defrecord Logger [config overrides pattern]

  component/Lifecycle

  (start [this]
    (log/infof "Starting Logger...")
    (unilog/start-logging!
      {:level     (:log-level config)
       :console   (case (:log-encoder config)
                    "json" {:encoder :json}
                    "pattern" {:encoder :pattern
                               :pattern pattern})
       :overrides overrides})
    (log/infof "Started Logger (%s)" (:log-level config))
    this)

  (stop [this]
    this))


;; more information regarding patterns at https://logback.qos.ch/manual/layouts.html
(def default-pattern "[%date] %highlight(%-5level) %logger %mdc{item-id} - %msg%n")

(def default-config
  {:log-level   "info"
   :log-encoder "pattern"})

(s/defschema LoggerConfig
  {:log-level   s/Str
   :log-encoder s/Str})

(defn new-logger [overrides pattern]
  (map->Logger {:overrides    overrides
                :config-shape LoggerConfig
                :config       default-config
                :pattern      (or pattern default-pattern)}))
