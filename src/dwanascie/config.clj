(ns dwanascie.config
  (:require [clojure.string :as string]
            [clojure.walk :as walk]
            [clojure.java.io :as io]
            [com.stuartsierra.component]
            [schema.core :as s]
            [schema-tools.core :as st]
            [yaml.core :as yaml])
  (:import com.stuartsierra.component.Lifecycle
           java.io.File))


;;; Configuration Protocol

(defprotocol Config

  (set-config [component config]
    "Config will be a map with partial configuration. The component is expected
     to validate and store this configuration, overwriting old config values.")

  (get-config [component]
    "Returns active configuration. N.b.: Returns default configuration when no
     configuration was overwritten via set-config.")

  (inject-secrets [component secrets]
    "Injects secret values from map secrets into active configuration."))



(defn deep-merge [a & maps]
  (if (map? a)
    (apply merge-with deep-merge a maps)
    (apply merge-with deep-merge maps)))

(defn update-config [component shape config]
  (->> shape
       (st/optional-keys-schema)
       (st/select-schema config)
       (update component :config deep-merge)))

(def ^:private secret-re "[A-Z_]+")

(defn- replace-secret-vars [secrets s]
  (if (string? s)
    (let [re (re-pattern (str "\\$[\\({]([A-Z_]+" secret-re ")[\\)}]"))
          vars (->> (re-seq re s)
                    (map second))]
      (reduce
        (fn [s secret-var]
          (if-let [val (get secrets secret-var)]
            (string/replace s (re-pattern (str "\\$[\\({]" secret-var "[\\)}]")) val)
            s))
        s
        vars))
    s))

(extend-type Lifecycle

  Config

  (get-config [this]
    (get this :config {}))

  (set-config [this config]
    (if-let [shape (not-empty (:config-shape this))]
      (update-config this shape config)
      this))

  (inject-secrets [this secrets]
    (update this :config #(walk/postwalk
                            (partial replace-secret-vars secrets)
                            %))))




;;; Config

(defn- env-name-to-path [env-name]
  (mapv (fn [part] (keyword (.toLowerCase (.replace ^String part "_" "-"))))
        (string/split env-name #"__")))

(defn- parse-env-string [^String env-string]
  (let [s (string/lower-case env-string)]
    (try
      (cond
        (= "false" s) false
        (= "true" s) true
        :else (Integer/valueOf s))
      (catch NumberFormatException _
        env-string))))

(defn- shared-config-from-env []
  (reduce-kv
    (fn [config name value]
      (let [path          (env-name-to-path name)
            shared-value? (= 1 (count path))
            parsed-value  (parse-env-string value)]
        (if-not shared-value?
          config
          (assoc config (first path) parsed-value))))
    {}
    (into {} (System/getenv))))

(defn- service-config-from-env [service-name]
  (reduce-kv
    (fn [config name value]
      (let [path            (env-name-to-path name)
            service-config? (= service-name (first path))
            parsed-value    (parse-env-string value)]
        (if-not service-config?
          config
          (assoc-in config (rest path) parsed-value))))
    {}
    (into {} (System/getenv))))

(defn valid-secret-name? [s]
  (re-matches (re-pattern secret-re) s))

(defn- secrets-from-dir [dir]
  (let [secret-file? #(and (.isFile ^File %)
                           (valid-secret-name? (.getName ^File %)))
        files (->> (file-seq dir)
                   (filter secret-file?))]
    (reduce
      #(assoc %1
         (.getName ^File %2)
         (-> (.getAbsolutePath ^File %2)
             (slurp)
             (string/trim)))
      {}
      files)))

(defn- secrets-from-file [file]
  (let [Secrets {(s/constrained s/Str valid-secret-name?) s/Str}]
    (try
      (->> (yaml/from-file file false)
           (s/validate Secrets))
      (catch Exception e
        (throw (ex-info "Failed to read secrets from file" {:path file} e))))))

(defn- read-secrets [path]
  (if-not (string/blank? path)
    (let [file (io/file path)]
      (if (.isDirectory file)
        (secrets-from-dir file)
        (secrets-from-file file)))
    {}))

(defn with-config [definition filename-or-map secrets-path]
  (let [file-config        (cond
                             (map? filename-or-map) filename-or-map
                             (not (string/blank? filename-or-map)) (yaml/from-file filename-or-map))
        service-name       (keyword (:service/name definition))
        shared-config      (:shared file-config {})
        shared-env-config  (shared-config-from-env)
        service-config     (get-in file-config [:services service-name] {})
        service-env-config (service-config-from-env service-name)
        secrets            (read-secrets secrets-path)]
    (update definition
            :service/components
            (fn [components]
              (reduce-kv
                (fn [system name component]
                  (let [component-default-config (or (get-config component) {})
                        component-config         (get service-config name {})
                        component-env-config     (get service-env-config name {})]
                    (assoc system name (-> component
                                           (set-config component-default-config)
                                           (set-config shared-config)
                                           (set-config shared-env-config)
                                           (set-config component-config)
                                           (set-config component-env-config)
                                           (inject-secrets secrets)))))
                {}
                components)))))

(defn- service-config [definition]
  (reduce-kv (fn [config name component]
               (assoc config name (get-config component)))
             {}
             (:service/components definition)))

(defn yaml-config [definition]
  (let [service-config (service-config definition)
        service-name   (:service/name definition)
        config         {:services {service-name service-config}}]
    (yaml/generate-string config :dumper-options {:flow-style :block})))