(ns dwanascie.metrics
  (:require [clojure.tools.logging :as log]
            [com.stuartsierra.component :as component]
            [iapetos.core :as p]
            [iapetos.collector.jvm :as jvm]
            [iapetos.collector.ring :as ring]
            [org.httpkit.server :as http-kit]
            [schema.core :as s]))


(def ^:dynamic registry (p/collector-registry))

(defn get-collector
  ([metric]
   (registry metric))
  ([metric labels]
   (registry metric labels)))

(def collector-fns
  {:counter   p/counter
   :gauge     p/gauge
   :histogram p/histogram})

(def CollectorKeys
  (apply s/enum (keys collector-fns)))

(s/defschema MetricsDefinition
  [(s/conditional
     #(= (first %) :counter) [(s/one CollectorKeys "counter")
                              (s/one s/Keyword "Registry name")
                              (s/one {(s/optional-key :description) s/Str
                                      (s/optional-key :lazy?)       s/Bool
                                      (s/optional-key :labels)      [s/Keyword]}
                                     "Option map")]
     #(= (first %) :gauge) [(s/one CollectorKeys "gauge")
                            (s/one s/Keyword "Registry name")
                            (s/one {(s/optional-key :description) s/Str
                                    (s/optional-key :lazy?)       s/Bool
                                    (s/optional-key :labels)      [s/Keyword]}
                                   "Option map")]
     #(= (first %) :histogram) [(s/one CollectorKeys "histogram")
                                (s/one s/Keyword "Registry name")
                                (s/one {(s/optional-key :lazy?)       s/Bool
                                        (s/optional-key :description) s/Str
                                        (s/optional-key :labels)      [s/Keyword]
                                        (s/optional-key :buckets)     [s/Num]}
                                       "Option map")])])

(defn- make-collectors [metrics]
  (s/validate MetricsDefinition metrics)
  (mapv #(apply (get collector-fns
                     (first %))
                (rest %))
        metrics))

(defn- wrap-health [handler status]
  (fn [{:keys [request-method uri] :as request}]
    (if (= uri "/ready")
      (if (= request-method :get)
        {:status (if (= @status :started)
                   200
                   503)}
        {:status 405})
      (handler request))))

(defn set-status [metrics status]
  (reset! (:system-status metrics) status))

(defrecord Metrics [config metrics stop-fn system-status]

  component/Lifecycle

  (start [this]
    (log/infof "Starting Metrics exporter on %s:%s" (:metrics-ip config) (:metrics-port config))
    (let [{:keys [metrics-ip metrics-port metrics-jvm]} config
          collectors    (make-collectors metrics)
          new-registry  (cond-> (apply p/register registry collectors)
                          metrics-jvm jvm/initialize)
          system-status (atom :starting)
          stop-fn       (http-kit/run-server (-> (constantly {:status 404})
                                                 (ring/wrap-metrics-expose registry)
                                                 (wrap-health system-status))
                                             {:port metrics-port
                                              :ip   metrics-ip})]
      (log/infof "Started Metrics exporter on %s:%s" (:metrics-ip config) (:metrics-port config))
      (alter-var-root #'registry (fn [_] new-registry))
      (assoc this
        :stop-fn stop-fn
        :system-status system-status)))

  (stop [this]
    (when stop-fn
      (log/infof "Stopping Metrics exporter on %s:%s..." (:metrics-ip config) (:metrics-port config))
      (stop-fn))
    (alter-var-root #'registry (fn [_] (p/collector-registry)))
    (log/infof "Stopped Metrics exporter on %s:%s" (:metrics-ip config) (:metrics-port config))
    (assoc this :stop-fn nil)))



(def default-config
  {:metrics-ip   "0.0.0.0"
   :metrics-port 9201
   :metrics-jvm  false})

(s/defschema MetricsConfig
  {:metrics-ip   s/Str
   :metrics-port s/Num
   :metrics-jvm  s/Bool})

(defn new-metrics [metrics]
  (map->Metrics {:config-shape MetricsConfig
                 :config       default-config
                 :metrics      metrics}))