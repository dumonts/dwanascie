(ns dwanascie.tracing
  (:require [clojure.string :as str]
            [clojure.tools.logging :as log]
            [com.stuartsierra.component :as component]
            [dwanascie.logging :as d12log]
            [schema.core :as s])
  (:import io.jaegertracing.Configuration
           io.opentracing.util.GlobalTracer
           (io.opentracing References
                           Scope
                           ScopeManager
                           Span
                           SpanContext
                           Tracer)
           (io.opentracing.propagation Format$Builtin
                                       TextMapAdapter)
           (java.util Map HashMap)))


;;; Global Tracer

(def tracer-properties-mapping
  {:jaeger-service-name              "JAEGER_SERVICE_NAME"
   :jaeger-agent-host                "JAEGER_AGENT_HOST"
   :jaeger-agent-port                "JAEGER_AGENT_PORT"
   :jaeger-endpoint                  "JAEGER_ENDPOINT"
   :jaeger-auth-token                "JAEGER_AUTH_TOKEN"
   :jaeger-user                      "JAEGER_USER"
   :jaeger-password                  "JAEGER_PASSWORD"
   :jaeger-propagation               "JAEGER_PROPAGATION"
   :jaeger-reporter-log-spans        "JAEGER_REPORTER_LOG_SPANS"
   :jaeger-reporter-max-queue-size   "JAEGER_REPORTER_MAX_QUEUE_SIZE"
   :jaeger-reporter-flush-interval   "JAEGER_REPORTER_FLUSH_INTERVAL"
   :jaeger-sampler-type              "JAEGER_SAMPLER_TYPE"
   :jaeger-sampler-param             "JAEGER_SAMPLER_PARAM"
   :jaeger-sampler-manager-host-port "JAEGER_SAMPLER_MANAGER_HOST_PORT"
   :jaeger-tags                      "JAEGER_TAGS"})

(definterface IMutableTracer
  (swap [^io.opentracing.Tracer new-tracer]))

(deftype MutableTracer [^:volatile-mutable ^Tracer delegate]
  Tracer
  (scopeManager [this]
    (when delegate
      (.scopeManager delegate)))

  (activeSpan [this]
    (when delegate
      (.activeSpan delegate)))

  (buildSpan [this operationName]
    (when delegate
      (.buildSpan delegate operationName)))

  (activateSpan ^{:added "0.26"} [this span]
    (when delegate
      (.activateSpan delegate span)))

  (inject [this spanContext format carrier]
    (when delegate
      (.inject delegate spanContext format carrier)))

  (extract [this format carrier]
    (when delegate
      (.extract delegate format carrier)))

  IMutableTracer
  (swap [this ^Tracer new-tracer]
    (set! delegate new-tracer)))

(def ^:dynamic *mutable-tracer* (->MutableTracer nil))

(defn- init-tracer! []
  (GlobalTracer/registerIfAbsent ^Tracer *mutable-tracer*))

(defn- ^Tracer global-tracer []
  (GlobalTracer/get))

(defn- active-span []
  (-> (global-tracer)
      (.activeSpan)))

(defn- swap-global-tracer! [^Tracer new-tracer]
  ;; FIXME when *mutable-tracer* is swapped, this will not actually register it as the global tracer
  (.swap ^MutableTracer *mutable-tracer* new-tracer))

(defn- jaeger-tracer [service-name config]
  (doseq [key (keys tracer-properties-mapping)]
    (let [val (get config key)]
      (when-not (nil? val)
        (System/setProperty (get tracer-properties-mapping key) (str val)))))
  (-> (Configuration/fromEnv service-name)
      (.getTracer)))


;; Span wire transport

(defn ^Map inject-map
  ([^Tracer tracer ^Span span]
   (when span
     (->> (HashMap.)
          (TextMapAdapter.)
          (.inject tracer (.context ^Span span) Format$Builtin/TEXT_MAP)
          (into {}))))
  ([]
   (inject-map (global-tracer) (active-span))))

(defn ^SpanContext extract-context
  ([^Tracer tracer ^Map headers]
   (when headers
     (.extract tracer Format$Builtin/TEXT_MAP (TextMapAdapter. headers))))
  ([headers]
   (extract-context (global-tracer) headers)))


;; Span

(defn log
  ([^Span span ^Map m]
   (-> span (.log m)))
  ([^Map m]
   (some-> (active-span) (log m))))

(defn set-tag
  ([^Span span ^String tag ^String val]
   (-> span (.setTag tag val)))
  ([^String tag ^String val]
   (some-> (active-span) (set-tag tag val))))

(defn set-baggage
  ([^Span span ^String k ^String v]
   (-> span (.setBaggageItem k v)))
  ([^String k ^String v]
   (some-> (active-span) (set-baggage k v))))

(defn get-baggage
  ([^Span span ^String k]
   (-> span (.getBaggageItem k)))
  ([^String k]
   (some-> (active-span) (get-baggage k))))

(defn ^Span span
  ([^Tracer tracer {:keys [op-name follows-from child-of tags]}]
   (let [child-context   (extract-context child-of)
         follows-context (extract-context follows-from)
         ^Span new-span  (cond-> (.buildSpan tracer op-name)
                                 follows-context (.addReference References/FOLLOWS_FROM follows-context)
                                 child-context (.addReference References/CHILD_OF child-context)
                                 true (.start))]
     (doseq [[k v] tags]
       (set-tag (name k) (pr-str v)))
     new-span))
  ([{:keys [op-name follows-from child-of tags] :as opts}]
   (span (global-tracer) opts)))

(defmacro with-span
  "Executes its body with an enclosing tracing-span.

  `opts` can be either the :op-name as string, or a span-opts-map"
  [opts & body]
  `(let [opts# (cond (string? ~opts) {:op-name ~opts}
                     (map? ~opts) ~opts)]
     (with-open [span# (-> ^Tracer (GlobalTracer/get)
                           ^ScopeManager (.scopeManager)
                           ^Scope (.activate ^Span (span opts#)))]
       (log/tracef "-> Span %s: %s" (:op-name opts#) (pr-str opts#))
       (let [start#  (System/currentTimeMillis)
             result# (do ~@body)
             stop#   (System/currentTimeMillis)]
         (log/tracef "<- Span %s: %s msecs" (:op-name opts#) (- stop# start#))
         result#))))

(defmacro with-span-time
  "Combines tracing and `time`-logging.
Wraps the `body` into `dwanascie.tracing/with-span`, while printing execution
time on `log-level` [:trace :debug ...]

`name-or-span-map` can be either a string with the tasks name or a span-context-map"
  [log-level name-or-span-map & body]
  `(cond
     (string? ~name-or-span-map)
     (d12log/with-time ~log-level ~name-or-span-map
       (with-span {:op-name (str/lower-case ~name-or-span-map)}
         ~@body))

     (and (map? ~name-or-span-map) (:op-name ~name-or-span-map))
     (d12log/with-time ~log-level (:op-name ~name-or-span-map)
       (with-span ~name-or-span-map
         ~@body))

     :else
     (log/warnf (IllegalArgumentException. "Not a valid name or span-map")
       "cannot process name-or-span-map as %s" ~name-or-span-map)))

;;; Component

(defrecord TracerComponent [service-name config]

  component/Lifecycle

  (start [this]
    (if (:tracing-enabled config)
      (do
        (log/infof "Starting Tracer...")
        (init-tracer!)
        (swap-global-tracer! (jaeger-tracer service-name config))
        (log/info "Started Tracer"))
      (log/infof "Skipped Tracer (disabled in config)"))
    this)

  (stop [this]
    (when (:tracing-enabled config)
      (log/info "Stopping Tracer...")
      (swap-global-tracer! nil)
      (log/info "Stopped Tracer"))
    this))

;; Config Docs: https://github.com/jaegertracing/jaeger-client-java/blob/v0.33.1/jaeger-core/README.md
(s/defschema TracerConfig
  {:tracing-enabled                  s/Bool
   :jaeger-service-name              (s/maybe s/Str)
   :jaeger-agent-host                (s/maybe s/Str)
   :jaeger-agent-port                (s/maybe s/Num)
   :jaeger-endpoint                  (s/maybe s/Str)
   :jaeger-auth-token                (s/maybe s/Str)
   :jaeger-user                      (s/maybe s/Str)
   :jaeger-password                  (s/maybe s/Str)
   :jaeger-propagation               (s/maybe s/Str)
   :jaeger-reporter-log-spans        (s/maybe s/Bool)
   :jaeger-reporter-max-queue-size   (s/maybe s/Num)
   :jaeger-reporter-flush-interval   (s/maybe s/Num)
   :jaeger-sampler-type              (s/maybe s/Str)
   :jaeger-sampler-param             (s/maybe s/Num)
   :jaeger-sampler-manager-host-port (s/maybe s/Str)
   :jaeger-tags                      (s/maybe s/Str)})

(def tracer-default-config
  (merge
    (zipmap (keys TracerConfig) (repeat nil))
    {:tracing-enabled      false
     :jaeger-agent-host    "localhost"
     :jaeger-agent-port    6831
     :jaeger-sampler-type  "const"
     :jaeger-sampler-param 1}))

(defn new-tracer [service-name]
  (map->TracerComponent {:service-name service-name
                         :config-shape TracerConfig
                         :config       tracer-default-config}))