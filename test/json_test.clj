(ns json-test
  (:require [clojure.test :refer :all]
            [dwanascie.json :refer :all]
            [schema-generators.generators :as g]
            [schema.core :as s])
  (:import (java.time Instant)
           (java.util UUID Date)
           (clojure.lang ExceptionInfo)))

(defn test-in-out [schema d]
  (let [conv (converter schema)
        json (edn->json conv d)
        edn (json->edn conv json)]
    #_(println "TEST: " (pr-str [json edn]))
    (= d edn)))

(defn test-factory
  "This function generate automatic tests with schema data
   factory-vec: [{s/Str s/Num} {s/Str s/Keyword} ...]
   opt:
   - test-mode = :validate, :validate-exception,:single,:single-with-value,:vec-with-validate :single-exception , :single-exception-with-value, :repeatedly ,:maps-vecs, :sets , :enums
   - dotimes-runs: Takes a Number for how often dotimes runs (> 0).
   Tip: Use a for loop like: (for [x [s/Str s/Uuid] y [s/Keyword s/Num] {x y})
   a example:
   - :vec-with-validate: (test-factory [s/Bool] {:test-mode :vec-wiht-validate :value-vec [true false]})
   - :single (test-factory [s/Int] {:test-mode :single :dotimes-runs 10})
   - :enums (test-factory (for [x [s/Keyword s/Str s/Uuid s/Symbol s/Int s/Num s/Bool] y [s/Keyword s/Str s/Num s/Uuid s/Int s/Bool s/Symbol]] {:value x :data [(s/one x \"x\") (s/one y \"y\")]}) {:test-mode :enums :dotimes-runs 10})
   "
  [factory-vec opt]
  (let [{:keys [dotimes-runs test-mode value value-vec rep-fn]} opt]
    (doseq [data factory-vec]
      (dotimes [_ (if-not (nil? dotimes-runs) dotimes-runs 1)]
        (case test-mode
          :validate (is (s/validate JSONCompatible data))
          :validate-exception (is (thrown? ExceptionInfo (s/validate JSONCompatible data)))
          :single (is (test-in-out data (g/generate data)))
          :single-with-value (is (test-in-out data value))
          :single-exception (is (thrown? ExceptionInfo (test-in-out data (g/generate data))))
          :single-exception-with-value (is (thrown? ExceptionInfo (test-in-out data value)))
          :vec-with-validate (do (is (s/validate JSONCompatible data)) (doseq [val value-vec] (is (test-in-out data val))))
          :repeatedly (doseq [val (take 100 (repeatedly rep-fn))]
                        (is (test-in-out data val)))
          :repeatedly-generate (doseq [val (take 100 (repeatedly #(g/generate rep-fn)))]
                                 (is (test-in-out data val)))
          :string-output-equals (doseq [val value-vec]
                                  (is (let [c (converter data) n val] (= (str (->> n (edn->json c) (json->edn c))) (str n)))))
          :maps-vecs (is (test-in-out data (clojure.walk/postwalk (fn [e] (if (= (str e) "NaN") 0 e)) (g/generate data))))
          :sets (is (test-in-out (set (flatten (vec data)))
                                 (set (flatten (vec (clojure.walk/postwalk (fn [e]
                                                                             (cond
                                                                               (= (str e) "NaN") 0
                                                                               :else e)) (g/generate data)))))))
          :enums (let [s (clojure.walk/postwalk (fn [e] (if (= (str e) "NaN") 0 e)) (g/generate (:data data)))]
                   (when-not (empty? s)
                     (is
                       (test-in-out
                         (s/constrained (:value data) #(contains? (set s) %))
                         (first (filter #(nil? (s/check (:value data) %)) (set s))))))))))))
(deftest atomic
  (test-factory [s/Bool] {:test-mode :vec-with-validate :value-vec [true false]})
  ;; Int test with With Max Long and BigInt
  (test-factory [s/Int] {:test-mode :vec-with-validate :value-vec [1 0xff 9223372036854775807 9223372036854775808]})
  (test-factory [s/Int] {:test-mode :single :dotimes-runs 10})
  (test-factory [s/Int] {:test-mode :single-exception-with-value :value Float/NaN})
  (test-factory [s/Int] {:test-mode :single-exception-with-value :value Double/NaN})
  ;Number test
  (test-factory [s/Num] {:test-mode :vec-with-validate :value-vec [Float/NEGATIVE_INFINITY Float/POSITIVE_INFINITY Double/NEGATIVE_INFINITY Double/POSITIVE_INFINITY]})
  (test-factory [s/Num] {:test-mode :string-output-equals :value Float/NaN})
  ;String test
  (test-factory [s/Str] {:test-mode :vec-with-validate :value-vec ["Hallo" "äüöß"  "!\"$%&/()=?'`´*'#+*!|°<>"]})
  (test-factory [s/Str] {:test-mode :single :dotimes-runs 10})
  ;Keyword test
  (test-factory [s/Keyword] {:test-mode :vec-with-validate :value-vec [:a :a/b]})
  (test-factory [s/Keyword] {:test-mode :single :dotimes-runs 10})
  ;Uuid test
  (test-factory [s/Uuid] {:test-mode :vec-with-validate :value-vec [(UUID/randomUUID) (UUID/randomUUID)]})
  (test-factory [s/Uuid] {:test-mode :single :dotimes-runs 10})
  ;Symbol test
  (test-factory [s/Symbol] {:test-mode :vec-with-validate :value-vec ['asym 'wertz]})
  (test-factory [s/Symbol] {:test-mode :single :dotimes-runs 10})
  ;Regex test
  (test-factory [s/Regex] {:test-mode :validate})
  (test-factory [s/Regex] {:test-mode :string-output-equals :value-vec [#"^http\\:\\/\\/[a-zA-Z0-9.-]+\\.[a-zA-Z]"
                                                                        #"^\\d+$"
                                                                        #"^articles/(?<year>[0-9]{4})/$"
                                                                        #"^profile/(?<username>[\\w.@+-]+)/$"
                                                                        #"articles/(?<year>[0-9]{4})/(?<month>[0-9]{2})/$"]})

  (test-factory [s/Inst] {:test-mode :vec-with-validate :value-vec [(Date.)]})
  ;;Todo dates like 1970-01-01T00:00:00.9Z makes issues
  (test-factory [s/Inst] {:test-mode :single :dotimes-runs 10})
  (test-factory [s/Inst] {:test-mode :repeatedly :rep-fn #(Date.)})
  (test-factory [s/Inst] {:test-mode :repeatedly-generate :rep-fn s/Inst})
  (test-factory [s/Inst] {:test-mode :repeatedly-generate :rep-fn Date})
  (test-factory [{s/Str  s/Inst}] {:test-mode :single-with-value :value {"1234" (Date.)}})
  (test-factory [{s/Inst s/Str}] {:test-mode :single-exception-with-value :value {(Date.) "hallo"}})
  (test-factory [{s/Inst s/Str}] {:test-mode :single-exception :dotimes-runs 10})
  (test-factory [Instant] {:test-mode :vec-with-validate :value-vec [(Instant/now)]})
  (test-factory [Instant] {:test-mode :repeatedly :rep-fn #(Instant/now)}))

(deftest map-keys
  (test-factory [{s/Str s/Str} {s/Keyword s/Str}
                 {s/Uuid s/Str} {s/Symbol s/Str}
                 {s/Regex s/Str} {Instant s/Str}
                 {:a s/Str :b s/Str} {:a s/Str (s/optional-key :b) s/Str}
                 {:a s/Str (s/required-key :b) s/Str}
                 {:a s/Str (s/optional-key :b) s/Str (s/required-key :c) s/Str}] {:test-mode :validate})

  (test-factory (into [{s/Bool s/Str} {s/Int s/Str}
                       {s/Num s/Str} {s/Inst s/Str}
                       {:a s/Str s/Str s/Str} {(s/optional-key "a") s/Str}
                       {{s/Str s/Str} s/Str} {[s/Str] s/Str}]
                      (for [x [s/Bool s/Int s/Num]
                            y [s/Keyword s/Str s/Num s/Int s/Bool s/Symbol s/Uuid]] {x y})) {:test-mode :validate-exception}))

(deftest map-vals

  (test-factory [{s/Keyword s/Str}] {:test-mode :single :dotimes-runs 10})

  (test-factory [{s/Str s/Bool} {s/Str s/Int}
                 {s/Str s/Num} {s/Str s/Str}
                 {s/Str s/Keyword} {s/Str s/Uuid}
                 {s/Str s/Symbol} {s/Str s/Regex}
                 {s/Str s/Inst} {s/Str Instant}
                 {s/Str {s/Str s/Bool}} {s/Str {:a [s/Str] (s/optional-key :b) s/Inst}}] {:test-mode :validate})

  (test-factory (for [x [s/Keyword s/Str s/Uuid s/Symbol]
                      y [s/Keyword s/Str s/Num s/Int s/Bool s/Symbol s/Uuid]] {x y}) {:test-mode :maps-vecs})

  (test-factory (for [x [s/Keyword s/Str s/Uuid s/Symbol]
                      y [s/Keyword s/Str s/Num s/Int s/Bool s/Symbol s/Uuid]] {x {x y}}) {:test-mode :maps-vecs})

  (test-factory (for [x [s/Keyword s/Str s/Uuid s/Symbol]
                      x2 [s/Keyword s/Str s/Uuid s/Symbol]
                      y [s/Keyword s/Str s/Num s/Int s/Bool s/Inst Date s/Symbol s/Uuid]] {x {x2 y}}) {:test-mode :maps-vecs}))

(deftest sets
  (test-factory [#{s/Str} #{[s/Str]}
                 #{{s/Keyword s/Str}}] {:test-mode :validate})

  (test-factory [#{s/Str}
                 #{[s/Str]}
                 #{{s/Keyword s/Str}}] {:test-mode :single :dotimes-runs 10})

  (test-factory (for [x [s/Keyword s/Str s/Uuid s/Symbol s/Int s/Num s/Bool]] #{x}) {:test-mode :sets :dotimes-runs 10})
  (test-factory (for [x [s/Keyword s/Str s/Uuid s/Symbol s/Int s/Num s/Bool]] #{[x]}) {:test-mode :sets :dotimes-runs 10})

  (test-factory (for [x [s/Keyword s/Str s/Uuid s/Symbol]
                      y [s/Keyword s/Str s/Num s/Int s/Bool s/Symbol s/Uuid]] #{{x y}}) {:test-mode :sets :dotimes-runs 10}))

(deftest vecs
  (test-factory [[s/Str] [(s/one s/Str "str")]
                 [(s/one s/Str "str") s/Keyword]
                 [(s/one s/Str "str") s/Keyword]] {:test-mode :validate})

  (test-factory [[s/Keyword]
                 [(s/one s/Keyword "kw")]
                 [(s/one s/Keyword "kw") s/Str]
                 [(s/one s/Keyword "kw") (s/optional s/Int "i") s/Str]] {:test-mode :single :dotimes-runs 10})

  (test-factory [[s/Keyword]] {:test-mode :single-with-value :value '(:a :b :c)})
  (test-factory (for [y [s/Keyword s/Str s/Num s/Int s/Bool s/Symbol s/Uuid]] [y]) {:test-mode :maps-vecs :dotimes-runs 10})

  (dotimes [_ 10]
    (let [s (vec (take 6 (repeatedly #(rand-nth [(s/one s/Keyword (str UUID)) (s/one s/Str (str UUID))
                                                 (s/one s/Num (str UUID)) (s/one s/Int (str UUID))
                                                 (s/one s/Bool (str UUID)) (s/one s/Symbol (str UUID))
                                                 (s/one s/Uuid (str UUID))]))))]
      (is (test-in-out s (clojure.walk/postwalk (fn [e] (if (= (str e) "NaN") 0 e)) (g/generate s)))))))

(deftest enums
  (test-factory [(s/constrained s/Str #{"a" "b"}) ] {:test-mode :validate})
  (test-factory [(s/enum "a" "b") ] {:test-mode :validate-exception})
  (test-factory [(s/constrained s/Str #{"a" "b"})] {:test-mode :single-exception-with-value :value "c"})
  (test-factory (for [x [s/Keyword s/Str s/Uuid s/Symbol s/Int s/Num s/Bool]
                      y [s/Keyword s/Str s/Num s/Uuid s/Int s/Bool s/Symbol]] {:value x :data [(s/one x "x") (s/one y "y")]}) {:test-mode :enums :dotimes-runs 10}))