(ns system_test
  (:require [clojure.test :as test]
            [com.stuartsierra.component :as component]
            [dwanascie.system :as system]
            [schema.core :as s]))

(s/defschema ConfigShape
  {:address s/Str
   :port    s/Num})

(defrecord TestComponent [config]
  component/Lifecycle
  (start [this]
    this)
  (stop [this]
    this))

(defn new-component []
  (map->TestComponent {:config-shape ConfigShape
                       :config       {:address "localhost" :port 8080}}))

(def definition
  {:service/name              "test"
   :service/type              "test"
   :service/version           "0.0.0"
   :service/components        {:api1    (new-component)
                               :api2    (new-component)
                               :backend (new-component)
                               :logger  (new-component)}
   :service.components/deps   {:api1 [:backend :logger]
                               :api2 [:logger]}
   :service.logging/overrides {"io.netty" :error}
   :service.cli/commands      {"db" {:subcommands {"migrate" {:fn          println
                                                              :description "Run database migrations"
                                                              :options     [["-h" "--host" "Postgres server hostname"]
                                                                            ["-p" "--port" "Postgres server port"]]}}}}})

(remove-method clojure.core/print-method com.stuartsierra.component.SystemMap)