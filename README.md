# 12-factor microservice framework

* read config from environment vars or yaml file
* logging to console via logback (unilog)
* runtime lifecyle management (components)
* simple cli interface
* web server (http-kit)
* prometheus metrics (iapetos)

### Modules

* dwanascie-datomic - datomic peer component
* dwanascie-jdbc - jdbc hugsql component (with postgresql driver)
* dwanascie-amqp - rabbitmq client (langohr) 
* dwanascie-auth - user authentication/authorization

## License

Distributed under the Eclipse Public License, the same as Clojure.
