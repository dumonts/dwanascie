(ns dwanascie.amqp
  (:require [cheshire.core :as cheshire]
            [clojure.core.async :as async]
            [clojure.java.io :as io]
            [clojure.tools.logging :as log]
            [cognitect.transit :as transit]
            [com.stuartsierra.component :as component]
            [dwanascie.tracing :as tracing]

            [langohr.basic :as lb]
            [langohr.channel :as lch]
            [langohr.confirm :as lcf]
            [langohr.consumers :as lcons]
            [langohr.core :as rmq]
            [langohr.exchange :as le]
            [langohr.queue :as lq]
            [schema.core :as s])
  (:import java.util.UUID
           java.util.concurrent.Executors
           (clojure.lang ExceptionInfo)
           (java.io ByteArrayOutputStream ByteArrayInputStream IOException)))

(defn timeout? [e]
  (= :timeout (-> e ex-data :error-type)))

(s/defschema AmqpErrorMessage
  {:error-type s/Keyword
   :error-id   s/Uuid
   (s/optional-key :error-data) s/Any})

(defn amqp-error-response [exception]
  (let [error-id (UUID/randomUUID)
        {:keys [error-data error-type]} (ex-data exception)]
    (if (instance? ExceptionInfo exception)
      (log/errorf exception "error-id: %s, error-type: %s, error-data: %s" error-id error-type (print-str error-data))
      (log/errorf exception "error-id: %s, error-type: %s" error-id error-type))
    {:error-type error-type
     :error-id   error-id
     :error-data error-data}))

(defn- error? [data]
  (and (:error-id data) (:error-type data)))

(defn serialize [data-schema content-type data]
  (s/validate (s/conditional error? AmqpErrorMessage :else data-schema) data)
  (let [out          (ByteArrayOutputStream.)
        content-type (or content-type "application/transit+json")]
    (case content-type
      "application/transit+json" (let [writer (transit/writer out :json)]
                                   (transit/write writer data))
      "application/json" (cheshire/generate-stream data (io/writer out)))
    (.toByteArray out)))


(defn deserialize [data-schema content-type ^bytes payload]
  (let [in   (ByteArrayInputStream. payload)
        data (case content-type
               "application/transit+json" (let [reader (transit/reader in :json)]
                                            (transit/read reader))
               "application/json" (cheshire/parse-stream in))]
    (s/validate (s/conditional error? AmqpErrorMessage :else data-schema) data)))

(defn- with-default-props [content-type props]
  (merge {:content-type     (or content-type "application/transit+json")
          :content-encoding "UTF-8"
          :mandatory        true}
         props))

;; Responder

(defn str-map [headers]
  (reduce (fn [m [k v]]
            (assoc m (str k) (str v)))
          {}
          headers))

(defn- responder-thread [this ch consumer-tag
                         {:keys [request-data-schema response-data-schema handler-fn] :as options}
                         {:keys [request-queue response-exchange filter-mode] :as _config}]
  (let [handler (fn [ch {:keys [delivery-tag reply-to correlation-id routing-key content-type headers] :as props} ^bytes payload]
                  (try
                    (tracing/with-span {:op-name  (if filter-mode "response-filter" "response")
                                        :child-of (str-map headers)
                                        :tags     {"component" "dwanascie-amqp"
                                                   "span.kind" "server"}}
                      (log/tracef "Preparing response: %s/%s" response-exchange consumer-tag)
                      (let [data-or-error    (try
                                               (deserialize request-data-schema content-type payload)
                                               (catch ExceptionInfo ei
                                                 (if (= :schema.core/error (:type (ex-data ei)))
                                                   (amqp-error-response (ex-info "Request payload does not match schema" (ex-data ei) ei))
                                                   (amqp-error-response (ex-info "Unexpected ExceptionInfo: deserialization failed" (ex-data ei) ei))))
                                               (catch Exception e
                                                 (amqp-error-response (ex-info "Request deserialization failed" (ex-data e) e))))
                            response         (try
                                               (handler-fn this data-or-error)
                                               (catch Exception e
                                                 (amqp-error-response (ex-info "Request handler failed" (ex-data e) e))))
                            out-content-type (:content-type options)
                            payload          (try
                                               (serialize response-data-schema out-content-type response)
                                               (catch ExceptionInfo ei
                                                 (if (= :schema.core/error (:type (ex-data ei)))
                                                   (throw (ex-info "Response payload does not match schema" (ex-data ei) ei))
                                                   (throw (ex-info "Unexpected ExceptionInfo: serialization failed" (ex-data ei) ei)))))
                            props            (cond-> {:persistent true}
                                                     correlation-id (assoc :correlation-id correlation-id)
                                                     filter-mode (assoc :reply-to reply-to
                                                                        :headers (tracing/inject-map)))
                            new-routing-key  (if filter-mode
                                               routing-key
                                               (or reply-to ""))]
                        (lb/publish ch response-exchange new-routing-key payload (with-default-props out-content-type props))
                        (log/tracef "Published response: %s/%s %s" response-exchange new-routing-key (pr-str props))
                        (lcf/wait-for-confirms ch)))
                    (catch Exception e                      ; last resort
                      (log/errorf e "Responder thread failed to publish response. Acking..."))
                    (finally
                      ;; clear messages in ANY case.
                      (try
                        (log/debugf "Acking message for %s" (print-str (select-keys props [:routing-key :exchange :correlation-id :reply-to])))
                        (lb/ack ch delivery-tag)            ; ack can not be checked for success, hence we can only check for IOExceptions
                        (catch IOException ioe
                          (log/error ioe "Acking failed - Retry once in 5s...")
                          (Thread/sleep 5000)               ; retry once to accommodate for network errors
                          (lb/ack ch delivery-tag))))))]
    (.start (Thread. ^Runnable (fn []
                                 (lcons/subscribe ch request-queue handler {:auto-ack     false
                                                                            :consumer-tag consumer-tag}))))))

(defn- prepare-responder [ch {:keys [request-exchange request-topic request-queue response-exchange consumer-tag filter-mode]}]
  (lcf/select ch)
  (le/declare ch request-exchange "topic")
  (lq/declare ch request-queue {:exclusive false :auto-delete false :durable true :arguments {"x-max-priority" 10}}) 
  (le/declare ch response-exchange "topic")
  (when (lq/bind ch request-queue request-exchange {:routing-key request-topic})
    (log/infof "AMQP Responder `%s` reads from `%s` via `%s` and writes items to `%s` %s"
      consumer-tag request-queue request-exchange response-exchange (if filter-mode 
                                                                      (str "with a routing key of " request-topic) 
                                                                      "via a queue set by the reply-to value of the request item"))))

(defn- start-consumer [{:keys [options connection] :as this} idx config]
  (let [new-chan (lch/open (:conn connection))]
    (lb/qos new-chan (:prefetch-count config))
    (prepare-responder new-chan config)
    (let [tag        (str (:consumer-tag config) "-" idx)
          new-thread (responder-thread this new-chan tag options config)]
      {:chan   new-chan
       :tag    tag
       :thread new-thread})))

(defn- stop-consumer [{:keys [chan tag]}]
  (when chan
    (lb/cancel chan tag)
    (rmq/close chan)))

(defrecord AmqpResponder [config options connection consumers tag]

  component/Lifecycle

  (start [this]
    (log/infof "Starting AMQP Responder %s..." (:consumer-tag config))
    (let [new-consumers (mapv #(start-consumer this % config)
                              (range (:consumer-count config)))]
      (log/infof "Started AMQP Responder %s" (:consumer-tag config))
      (assoc this :consumers new-consumers
                  :tag (:consumer-tag config))))

  (stop [this]
    (log/infof "Stopping AMQP Responder %s..." tag)
    (doseq [c consumers]
      (stop-consumer c))
    (log/infof "Stopped AMQP Responder %s" tag)
    (assoc this :consumers nil)))



(s/defschema AmqpResponderConfig
  {:consumer-count    s/Num
   :prefetch-count    s/Num
   :request-exchange  s/Str
   :request-queue     s/Str
   :request-topic     s/Str
   :consumer-tag      s/Str
   :response-exchange s/Str
   :filter-mode       s/Bool})

(def amqp-responder-default-config
  {:consumer-count    1
   :prefetch-count    1
   :request-exchange  "request-exchange"
   :request-queue     "request-queue"
   :request-topic     "request-topic"
   :consumer-tag      "consumer-tag"
   :response-exchange "response-exchange"
   :filter-mode       false})

(defn new-amqp-responder
  "What differs the AMQP responder from an AMQP Requester is its ability to consume items from
   a `request-queue`, apply a function to the items payload and, when in filter mode, proxy
   the response to another exchange. A third listening service can then consume the precessed
   item from a queue bound to this proxy exchange.

   When filter mode is not used, the response is simply sent to a reply exchange."
  ([options]
   (new-amqp-responder options {}))
  ([options default-config]
   (map->AmqpResponder {:options      options
                        :config-shape AmqpResponderConfig
                        :config       (merge amqp-responder-default-config default-config)})))

;; Requester

(defn request [requester routing-key data properties timeout-ms]
  ((:request-fn requester) routing-key data properties timeout-ms))

(defn- reply-topic [id]
  (str "reply." id))

(defn- request-fn [ch dispatch id {:keys [request-exchange response-exchange]} {:keys [request-data-schema content-type]}]
  (fn request [routing-key data properties timeout-ms]
    (tracing/with-span {:op-name "request"
                        :tags    {"component" "dwanascie-amqp"
                                  "span.kind" "client"}}
      (let [payload        (serialize request-data-schema content-type data)
            correlation-id (str (UUID/randomUUID))
            headers        (tracing/inject-map)
            props          (merge properties
                                  {:reply-to       (reply-topic id)
                                   :correlation-id correlation-id
                                   :expiration     (str timeout-ms)
                                   :headers        headers})
            response-chan  (async/chan)]
        (swap! dispatch assoc correlation-id response-chan)
        (log/tracef "AMQP request: %s/%s" request-exchange routing-key)
        (lb/publish ch request-exchange routing-key payload (with-default-props content-type props))
        (try
          (let [[response chan] (async/alts!! [response-chan (async/timeout timeout-ms)])]
            (async/close! response-chan)
            (if (= chan response-chan)
              (if (:error-type response)
                (throw (ex-info "AMQP request failed" response))
                response)
              (throw (ex-info "Timeout waiting for response" {:exchange       response-exchange
                                                               :error-type     :timeout
                                                               :reply-to       (reply-topic id)
                                                               :correlation-id correlation-id}))))
          (catch InterruptedException e
            (log/warn e "AMQP request interrupted"))
          (finally
            (swap! dispatch dissoc correlation-id)))))))



(defn- response-handler [ch dispatch id {:keys [consumer-tag]} {:keys [response-data-schema]}]
  (let [handler (fn [ch {:keys [delivery-tag correlation-id content-type]} ^bytes payload]
                  (log/tracef "Got response: %s/%s" consumer-tag id)
                  (let [data (deserialize response-data-schema content-type payload)
                        chan (get @dispatch correlation-id)]
                    (if chan
                      (async/>!! chan data)
                      (log/infof "Unroutable message received: %s" correlation-id)))
                  (lb/ack ch delivery-tag))]
    (.start (Thread. (fn []
                       (lcons/subscribe ch id handler {:auto-ack     false
                                                       :consumer-tag consumer-tag}))))))

(defn- prepare-requester [ch id {:keys [request-exchange response-exchange consumer-tag routing-key]}]
  (let [reply-key (reply-topic id)]
    (le/declare ch request-exchange "topic")
    (le/declare ch response-exchange "topic")
    (lq/declare ch id {:exclusive true :auto-delete true :durable false :arguments {"x-max-priority" 10}})
    (when (lq/bind ch id response-exchange {:routing-key reply-key})
      (log/infof "AMQP Requester `%s` writes items to `%s` with a routing key of `%s` and reads from reply queue `%s` via `%s`"
        consumer-tag request-exchange routing-key (reply-topic id) response-exchange))))

(defrecord AmqpRequester [config options connection chan tag thread dispatch]

  component/Lifecycle

  (start [this]
    (log/infof "Starting AMQP Requester %s..." (:consumer-tag config))
    (let [new-chan     (lch/open (:conn connection))
          new-dispatch (atom {})
          requester-id (str (UUID/randomUUID))]
      (lb/qos new-chan (:prefetch-count config))
      (prepare-requester new-chan requester-id config)
      (let [new-thread (response-handler new-chan new-dispatch requester-id config options)
            new-fn     (request-fn new-chan new-dispatch requester-id config options)]
        (log/infof "Started AMQP Requester %s" (:consumer-tag config))
        (assoc this :chan new-chan
                    :tag (:consumer-tag config)
                    :thread new-thread
                    :dispatch new-dispatch
                    :request-fn new-fn))))

  (stop [this]
    (when chan
      (log/infof "Stopping AMQP Requester %s..." tag)
      (lb/cancel chan tag)
      (rmq/close chan))
    (log/infof "Stopped AMQP Requester %s" tag)
    (assoc this :chan nil
                :tag nil
                :thread nil
                :dispatch nil
                :request-fn nil)))

(s/defschema AmqpRequesterConfig
  {:prefetch-count    s/Num
   :consumer-tag      s/Str
   :request-exchange  s/Str
   :response-exchange s/Str
   :routing-key       s/Str})

(def amqp-requester-default-config
  {:prefetch-count    1
   :consumer-tag      "consumer-tag"
   :routing-key       "request-queue"
   :request-exchange  "request-exchange"
   :response-exchange "response-exchange"})

(defn new-amqp-requester
  "This object exposes the method `request` to send items to a topic exchange `request-exchange`.
   Another queue and an exchange for dispatching responses serves as the reply queue. To consume this reply queue,
   a long living thread is spawned during initialization. To asynchronously correlate a request to a response, a
   random ID is attached to the payload and shared between the requesting and responding thread.

   Messages sent by another AMQP object are acked when the response handler executes successfully."
  ([options]
   (new-amqp-requester options {}))
  ([{:keys [_request-data-schema _response-data-schema _content-type] :as options} default-config]
   (map->AmqpRequester {:config       (merge amqp-requester-default-config default-config)
                        :config-shape AmqpRequesterConfig
                        :options      options})))


;; Consumer

(defn consume [{:keys [fn] :as this}]
  (if (fn? fn)
    (fn)
    (throw (ex-info "AMQP Consumer: consume called while not in pull-mode" {}))))

(defn- consumer-fn [this ch {:keys [consumer-queue consumer-tag]} {:keys [data-schema]}]
  (fn []
    (let [[{:keys [delivery-tag content-type headers]} ^bytes payload] (lb/get ch consumer-queue false)]
      (when payload
        (tracing/with-span {:op-name      "consume"
                            :follows-from (str-map headers)
                            :tags         {"component" "dwanascie-amqp"
                                           "span.kind" "consumer"}}
          [(deserialize data-schema content-type payload)
           #(lb/ack ch delivery-tag)])))))

(defn- consumer-thread [this ch {:keys [consumer-queue consumer-tag]} {:keys [data-schema handler-fn]}]
  (let [handler (fn [ch {:keys [delivery-tag content-type headers]} ^bytes payload]
                  (tracing/with-span {:op-name      "consume"
                                      :follows-from (str-map headers)
                                      :tags         {"component" "dwanascie-amqp"
                                                     "span.kind" "consumer"}}
                    (log/tracef "Consuming item: %s/%s" consumer-tag consumer-queue)
                    (->> (deserialize data-schema content-type payload)
                         (handler-fn this))
                    (lb/ack ch delivery-tag)))]
    (.start (Thread. ^Runnable (fn []
                                 (lcons/subscribe ch consumer-queue handler {:auto-ack     false
                                                                             :consumer-tag consumer-tag}))))))

(defn- prepare-consumer [ch {:keys [consumer-exchange consumer-topic consumer-queue consumer-tag]}]
  (le/declare ch consumer-exchange "topic")
  (lq/declare ch consumer-queue {:exclusive false :auto-delete false :durable true :arguments {"x-max-priority" 10}})
  (when (lq/bind ch consumer-queue consumer-exchange {:routing-key consumer-topic})
    (log/infof "AMQP Consumer `%s` reads from `%s` bound to `%s` via `%s`" consumer-tag consumer-queue consumer-topic consumer-exchange)))

(defrecord AmqpConsumer [config connection options chan tag thread]

  component/Lifecycle

  (start [this]
    (log/infof "Starting AMQP Consumer %s..." (:consumer-tag config))
    (let [new-chan (lch/open (:conn connection))]
      (lb/qos new-chan (:prefetch-count config))
      (prepare-consumer new-chan config)
      (let [new-consumer (if (:pull-mode? options)
                           (consumer-fn this new-chan config options)
                           (consumer-thread this new-chan config options))]
        (log/infof "Started AMQP Consumer %s" (:consumer-tag config))
        (assoc this :chan new-chan
                    :tag (:consumer-tag config)
                    (if (:pull-mode? options) :fn :thread) new-consumer))))

  (stop [this]
    (when chan
      (log/infof "Stopping AMQP Consumer %s..." tag)
      (lb/cancel chan tag)
      (rmq/close chan))
    (log/infof "Stopped AMQP Consumer %s" tag)
    (assoc this :chan nil
                :tag nil
                :thread nil
                :fn nil)))


(s/defschema AmqpConsumerConfig
  {:prefetch-count    s/Num
   :consumer-tag      s/Str
   :consumer-exchange s/Str
   :consumer-topic    s/Str
   :consumer-queue    s/Str})

(def amqp-consumer-default-config
  {:prefetch-count    1
   :consumer-tag      "consumer-tag"
   :consumer-exchange "consumer-exchange"
   :consumer-topic    "consumer-topic"
   :consumer-queue    "consumer-queue"})

(defn new-amqp-consumer
  "An AMQP Consumer does as the name suggests. It consumes items from a named queue identified by `consumer-queue`
  which is bound to an exchange `consumer-exchange`. The `consumer-tag` can be used to identify individual consuming threads`

  This can be used in chain with an AMQP responder when running in `filter-mode`. The routing key of all messages
  transported by the responder is used to publish to an exchange and ultimately to a queue.

  Attach the consumer to the same exchange and set the :consumer-key to the responding AMQP objects routing-key to start consuming messages."
  ([options]
   (new-amqp-consumer options {}))
  ([{:keys [data-schema pull-mode? handler-fn] :as options} default-config]
   (map->AmqpConsumer {:config-shape AmqpConsumerConfig
                       :config       (merge amqp-consumer-default-config default-config)
                       :options      options})))


;; Publisher

(defn publish [publisher routing-key data props]
  (tracing/with-span {:op-name "publish"
                      :tags    {"component" "dwanascie-amqp"
                                "span.kind" "producer"}}
    (let [ch           (:chan publisher)
          exchange     (:exchange publisher)
          data-schema  (get-in publisher [:options :data-schema])
          content-type (get-in publisher [:options :content-type])
          payload      (serialize data-schema content-type data)
          merged-props (->> (tracing/inject-map)
                            (assoc props :headers)
                            (with-default-props content-type))]
      (log/tracef "AMQP publish %s/%s" exchange routing-key)
      (lb/publish ch exchange routing-key payload merged-props)
      (lcf/wait-for-confirms ch))))

(defn- prepare-publisher [ch {:keys [publisher-exchange]}]
  (lcf/select ch)
  (le/declare ch publisher-exchange "topic"))

(defrecord AmqpPublisher [config connection options chan]

  component/Lifecycle

  (start [this]
    (log/info "Starting AMQP Publisher...")
    (let [new-chan (lch/open (:conn connection))]
      (prepare-publisher new-chan config)
      (log/info "Started AMQP Publisher")
      (assoc this :chan new-chan
                  :exchange (:publisher-exchange config))))

  (stop [this]
    (when chan
      (log/info "Stopping AMQP Publisher...")
      (rmq/close chan))
    (log/info "Stopped AMQP Publisher")
    (assoc this :chan nil
                :exchange nil)))



(s/defschema AmqpPublisherConfig
  {:publisher-exchange s/Str})

(def amqp-publisher-default-config
  {:publisher-exchange "publisher-exchange"})

(defn new-amqp-publisher
  ([options]
   (new-amqp-publisher options {}))
  ([{:keys [data-schema content-type] :as options} default-config]
   (map->AmqpPublisher {:config-shape AmqpPublisherConfig
                        :config       (merge amqp-publisher-default-config default-config)
                        :options      options})))


;; Channel

(defrecord AmqpChannel [config connection options chan]

  component/Lifecycle

  (start [this]
    (log/info "Opening AMQP Channel... ")
    (let [new-chan (lch/open (:conn connection))]
      (lb/qos new-chan (:prefetch-count config))
      (log/info "Opened AMQP Channel")
      (assoc this :chan new-chan)))

  (stop [this]
    (when chan
      (log/info "Closing AMQP Channel...")
      (rmq/close chan))
    (log/info "Closed AMQP Channel")
    (assoc this :chan nil)))



(s/defschema AmqpChannelConfig
  {:prefetch-count s/Num})

(def amqp-channel-default-config
  {:prefetch-count 1})

(defn new-amqp-channel
  "Wraps a newly created AMQP channel. Channels should not be shared across threads as referred to in `https://www.rabbitmq.com/channels.html`"
  ([]
   (new-amqp-channel {}))
  ([default-config]
   (map->AmqpChannel {:config-shape AmqpChannelConfig
                      :config       (merge amqp-channel-default-config default-config)})))


;; Connection

(defn- connect
  ([config]
   (connect config 1))
  ([{:keys [amqp-uri amqp-connection-name max-attempts retry-delay-ms] :as config} attempt]
   (let [max-attempts   (or max-attempts 30)
         retry-delay-ms (or retry-delay-ms 2000)]
     (try
       (rmq/connect {:uri             amqp-uri
                     :connection-name amqp-connection-name
                     :executor        (Executors/newCachedThreadPool)})
       (catch Exception e
         (if (<= attempt max-attempts)
           (do
             (log/infof "Connecting AMQP, attempt %d/%d..." attempt max-attempts)
             (Thread/sleep retry-delay-ms)
             (connect config (inc attempt)))
           (do
             (log/error e "Failed to connect AMQP")
             (throw (ex-info "Failed to connect AMQP" {} e)))))))))

(defrecord AmqpConnection [config conn]

  component/Lifecycle

  (start [this]
    (log/info "Opening AMQP Connection...")
    (let [new-conn (connect config)]
      (log/info "Opened AMQP Connection")
      (assoc this :conn new-conn)))

  (stop [this]
    (when conn
      (log/info "Closing AMQP Connection...")
      (rmq/close conn))
    (log/info "Closed AMQP Connection")
    (assoc this :conn nil)))

(s/defschema AmqpConnectionConfig
  {:amqp-connection-name s/Str
   :amqp-uri             s/Str})

(def amqp-connection-default-config
  {:amqp-uri             "amqp://localhost"
   :amqp-connection-name ""})

(defn new-amqp-connection
  "Wraps a shareable rabbitMQ connection"
  ([]
   (new-amqp-connection {}))
  ([default-config]
   (map->AmqpConnection {:config-shape AmqpConnectionConfig
                         :config       (merge amqp-connection-default-config default-config)})))
