(ns dwanascie.api
  (:require [clojure.core.async :as async]
            [clojure.string :as str]
            [clojure.tools.logging :as log]
            [cognitect.transit :as transit]
            [com.stuartsierra.component :as component]
            [dwanascie.metrics :refer [get-collector]]
            [dwanascie.tracing :as tracing]
            [dwanascie.util :as util]
            [iapetos.core :as p]
            [langohr.basic :as lb]
            [langohr.channel :as lch]
            [langohr.confirm :as lcf]
            [langohr.consumers :as lcons]
            [langohr.core :as rmq]
            [langohr.exchange :as le]
            [langohr.queue :as lq]
            [org.httpkit.client :as http]
            [schema.core :as s])
  (:import java.net.URI
           java.util.UUID
           java.util.concurrent.Executors
           (java.io ByteArrayOutputStream ByteArrayInputStream)
           (java.time Duration LocalDateTime)
           (java.util.concurrent TimeoutException)))


;; Schema

(def Operation s/Str)

(s/defschema Data
  {s/Any s/Any})

(s/defschema MessageBase
  {:api/group    s/Str
   :api/version  s/Str
   (s/optional-key :api/explicit-version) s/Str
   :message/path [(s/one Operation "first op") Operation]
   :message/data Data
   :message/priority (s/constrained s/Int #(< 0 % 11))})

(s/defschema ReplyTo
  [(s/one s/Str "client id") (s/optional s/Str "request id")])

(s/defschema RequestMessage
  (merge MessageBase
         {:message/kind     (s/eq "request")
          :message/reply-to ReplyTo}))

(s/defschema ErrorData
  {:error/message                   s/Str
   :error/type                      s/Str
   (s/optional-key :error/sub-type) s/Str
   (s/optional-key :error/data)     s/Any})

(s/defschema ReplyMessageFailed
  (-> MessageBase
      (merge
        {:message/kind     (s/eq "reply")
         :message/reply-to ReplyTo
         :message/status   (s/eq "failed")
         :message/data     ErrorData})))

(defn- reply-failed? [data]
  (and (= "reply" (:message/kind data))
       (= "failed" (:message/status data))))

(s/defschema ReplyMessage
  (merge MessageBase
         {:message/kind     (s/eq "reply")
          :message/reply-to ReplyTo
          :message/status   (s/eq "ok")}))

(defn- reply-ok? [data]
  (and (= "reply" (:message/kind data))
       (= "ok" (:message/status data))))

(s/defschema PubMessage
  (merge MessageBase
         {:message/kind (s/eq "publish")}))

(s/defschema RpcOperationSchema
  {:in  Data
   :out Data})

(defn rpc-schema? [data]
  (and (:in data) (:out data)))

(s/defschema ClientPubOperationSchema
  {:in Data})

(defn client-pub-schema? [data]
  (:in data))

(s/defschema ServerPubOperationSchema
  {:out Data})

(defn server-pub-schema? [data]
  (:out data))

(s/defschema OperationName
  (s/constrained s/Str #(not (or (str/ends-with? % "-pub")
                                 (str/ends-with? % "-sub")))))

(s/defschema ApiSchema
  {:api/group      s/Str
   :api/version    s/Str
   :api/operations {OperationName (s/conditional
                                   rpc-schema? RpcOperationSchema
                                   client-pub-schema? ClientPubOperationSchema
                                   server-pub-schema? ServerPubOperationSchema)}})

(defn op-schemas [api-schema op]
  (get-in api-schema [:api/operations op]))

(defn in-schema [schema op]
  (:in (op-schemas schema op)))

(defn out-schema [schema op]
  (:out (op-schemas schema op)))


;; Connection

(def delimiter "/")

(defn- prefixed [{:keys [api/group api/version]} s]
  (str "api" delimiter group delimiter "v" version s))

(defn- name-in-exchange [api-schema]
  (prefixed api-schema (str delimiter "in")))

(defn- name-out-exchange [api-schema]
  (prefixed api-schema (str delimiter "out")))

(defn- name-operation-queue [api-schema op]
  (prefixed api-schema (str delimiter op)))

(defn- declare-exchanges [{:keys [api-channel] :as ctx} api-schema]
  (let [in (name-in-exchange api-schema)
        out (name-out-exchange api-schema)]
    (le/declare api-channel in "topic")
    (le/declare api-channel out "topic")
    (assoc ctx
      :in-exchange in
      :out-exchange out
      :schema api-schema)))

(defn- connect
  ([ctx api-schema config]
   (connect ctx api-schema config 1))
  ([ctx api-schema {:keys [amqp-uri amqp-connection-name amqp-connect-max-attempts amqp-connect-retry-delay-ms] :as config} attempt]
   (s/validate ApiSchema api-schema)
   (try
     (let [conn (rmq/connect {:uri             amqp-uri
                              :connection-name amqp-connection-name
                              :executor        (Executors/newCachedThreadPool)})
           ch   (lch/open conn)]
       (lb/qos ch 1)
       (lcf/select ch)
       (log/info "Connected to RabbitMQ")
       (-> ctx
           (assoc :connection conn
                  :api-channel ch)
           (declare-exchanges api-schema)))
     (catch Exception e
       (if (<= attempt amqp-connect-max-attempts)
         (do
           (log/infof "Connecting to RabbitMQ, attempt %d/%d..." attempt amqp-connect-max-attempts)
           (Thread/sleep amqp-connect-retry-delay-ms)
           (connect ctx api-schema config (inc attempt)))
         (do
           (log/fatal e "Failed to connect to RabbitMQ")
           (throw (ex-info "Failed to connect to RabbitMQ" {} e))))))))

(defn- disconnect [{:keys [api-channel connection] :as ctx}]
  (when (and api-channel connection)
    (log/info "Disconnecting RabbitMQ...")
    (rmq/close api-channel)
    (rmq/close connection))
  (log/info "Disconnected RabbitMQ")
  (dissoc ctx :connection :api-channel :in-exchange :out-exchange :schema))


;; Serialization

(def api-content-type "application/transit+json")

(def api-content-encoding "UTF-8")

(defn- serialize [data]
  (tracing/with-span {:op-name "serialize"}
    (let [out    (ByteArrayOutputStream.)
          writer (transit/writer out :json)]
      (transit/write writer data)
      (.toByteArray out))))

(defn- deserialize [^bytes payload]
  (let [in     (ByteArrayInputStream. payload)
        reader (transit/reader in :json)]
    (transit/read reader)))

(defn- fix-headers [headers]
  (reduce (fn [m [k v]]
            (assoc m (str k) (str v)))
          {}
          headers))


;; Operations Helper

(defn- client-pub-op? [schema op]
  (= (set (keys (op-schemas schema op)))
     #{:in}))

(defn- server-pub-op? [schema op]
  (= (set (keys (op-schemas schema op)))
     #{:out}))

(defn- rpc-op? [schema op]
  (= (set (keys (op-schemas schema op)))
     #{:in :out}))

(defn- client-pub-path? [schema op-path]
  (and (every? #(rpc-op? schema %) (butlast op-path))
       (client-pub-op? schema (last op-path))
       (= (count op-path) (count (set op-path)))))

(defn- rpc-path? [schema op-path]
  (and (every? #(rpc-op? schema %) op-path)
       (= (count op-path) (count (set op-path)))))

(defn- next-op [path op]
  (->> path
       (drop-while #(not= op %))
       (second)))

(defn get-rpc-ops [operations]
  (filter (fn [operation]
            (let [op-spec (val operation)]
              (and (:in op-spec) (:out op-spec))))
          operations))

(defn- rpc-ops [schema]
  (->> (:api/operations schema)
       (get-rpc-ops)
       (map first)))

(defn- server-pub-ops [schema]
  (->> (:api/operations schema)
       keys
       (filter #(server-pub-op? schema %))))

(defn op-collector [ctx op metric]
  (get-collector
    metric
    {:op op
     :group (get-in ctx [:api-schema :api/group])}))


;; Validations

(defn- validate [schema data fail-msg]
  (try
    (s/validate schema data)
    (catch Exception e
      (throw (ex-info (str "Validation failed: " fail-msg) {} e)))))

(defn- validate-meta [{:keys [content-type content-encoding] :as meta}]
  (when-not (= content-type api-content-type)
    (throw (ex-info "Unknown content type received" {})))
  (when-not (= content-encoding api-content-encoding)
    (throw (ex-info "Unknown content encoding received" {}))))

(defn- validate-version [{expected :api/version} {provided :api/version :as msg}]
  (when-not (= expected provided)
    (throw (ex-info "Invalid API version" {:expected expected
                                            :provided provided})))
  msg)

(defn- client-pub-msg? [api-schema msg]
  (and (= (:message/kind msg) "publish")
       (client-pub-path? api-schema (:message/path msg))))

(defn- rpc-msg? [api-schema msg]
  (and (= (:message/kind msg) "request")
       (rpc-path? api-schema (:message/path msg))))

(defn- validate-in-data [api-schema op msg]
  (validate
    (in-schema api-schema op)
    (:message/data msg)
    (format "invalid input for op %s" op)))

(defn- validate-incoming [api-schema msg]
  (let [msg-schema (cond
                     (client-pub-msg? api-schema msg) PubMessage
                     (rpc-msg? api-schema msg) RequestMessage
                     :else (throw (ex-info "Invalid message kind" {})))]
    (->> msg
         (s/validate msg-schema)
         (validate-version api-schema))))

(defn- validate-reply [api-schema op msg]
  (let [data-schema (out-schema api-schema op)
        schema      (s/conditional
                     reply-failed? ReplyMessageFailed
                     reply-ok? (assoc ReplyMessage :message/data data-schema))]
    (validate
      schema
      msg
      (str "invalid reply for op " op))
    (validate-version api-schema msg)))

(defn- validate-server-pub [api-schema op msg]
  (let [data-schema (out-schema api-schema op)
        schema      (assoc PubMessage :message/data data-schema)]
    (validate
      schema
      msg
      (format "invalid pub for op %s" op))
    (validate-version api-schema msg)))


;; common

(defn- failed-msg [msg exception-data]
  (merge
    msg
    {:message/kind   "reply"
     :message/status "failed"
     :message/data   exception-data}))

(defn- op-route->str
  "stringify msg to api-group/api-version/message-path-endpoint"
  [{:keys [api/group api/version message/path]}]
  (if (= 1 (count path))
    (format "%s/%s/%s" group version (first path))
    (do (log/warnf ":message/path should have length 1, but was %s" (pr-str path))
        (format "%s/%s/%s" group version (pr-str path)))))


;; Server

(def default-priority 1)


(defn- amqp-send
  ([channel exchange op msg meta]
   (amqp-send channel exchange op msg meta nil 0))

  ([channel exchange op msg meta timeout-s max-retries]
   (tracing/with-span {:op-name "amqp-send"}
     (log/debugf "amqp-send API message: %s" (op-route->str msg))
     (log/tracef "amqp-send API message: %s" (pr-str msg))
     (lb/publish channel exchange op (serialize msg) (assoc meta :priority (:message/priority msg)))
     (tracing/log {"published" (util/get-timestamp)
                   "priority"  (str (:message/priority msg))})
     (if-not timeout-s
       (lcf/wait-for-confirms channel)

       (let [remaining-retries (atom (max 0 (or max-retries 0)))]
         (while (>= @remaining-retries 0)
           ; Initially tried this with loop/recur, but apparently we cannot recur from inside a (catch... to a
           ; loop point that is outside of the (try...
           (let [ts-before (LocalDateTime/now)]
             (try
               (lcf/wait-for-confirms channel (* 1000 timeout-s))
               (reset! remaining-retries -1)

               (catch TimeoutException te
                 (log/warnf te "Publishing message to %s timed out after %s seconds (%d retries remaining)"
                   (op-route->str msg)
                   (.getSeconds (Duration/between ts-before (LocalDateTime/now)))
                   @remaining-retries)
                 (swap! remaining-retries dec)
                 (when (< @remaining-retries 0)
                   (throw te)))))))))))


(defn- default-meta [with-headers?]
  (cond-> {:content-type     api-content-type
           :content-encoding api-content-encoding
           :mandatory        true
           :persistent       true}
    with-headers? (assoc :headers (tracing/inject-map))))

(defn server-pub
  ([ctx op data]
   (server-pub ctx op data {}))
  ([{:keys [schema api-channel out-exchange] :as ctx} op data {:keys [priority] :as options}]
   (tracing/with-span {:op-name (str "publish" delimiter op)
                       :tags    {"component" "dwanascie-api"
                                 "span.kind" "producer"}}
     (validate
       (out-schema schema op)
       data
       (str "invalid output data for op " op))
     (let [msg {:api/group        (:api/group schema)
                :api/version      (:api/version schema)
                :message/kind     "publish"
                :message/path     [op]
                :message/data     data
                :message/priority (or priority default-priority)}]
       (amqp-send api-channel out-exchange op msg (default-meta true))))))

(defn- span-map [msg headers op last-op?]
  (if (= (:message/kind msg) "request")
    (if last-op?
      {:op-name  (str "response" delimiter op)
       :child-of (fix-headers headers)
       :tags     {"component" "dwanascie-api"
                  "span.kind" "server"}}
      {:op-name  (str "request" delimiter op)
       :child-of (fix-headers headers)
       :tags     {"component" "dwanascie-api"
                  "span.kind" "client"}})
    (if last-op?
      {:op-name      (str "consume" delimiter op)
       :follows-from (fix-headers headers)
       :tags         {"component" "dwanascie-api"
                      "span.kind" "consumer"}}
      {:op-name      (str "publish" delimiter op)
       :follows-from (fix-headers headers)
       :tags         {"component" "dwanascie-api"
                      "span.kind" "producer"}})))

(defn- reply-routing-key [msg op]
  (let [[client-id request-id] (:message/reply-to msg)]
    (str op "." client-id (when request-id (str "." request-id)))))

(defn- reply-msg [{:keys [schema] :as ctx} op msg handler]
  (try
    (validate-in-data schema op msg)
    (assoc msg
      :message/data (s/validate
                      (out-schema schema op)
                      (handler ctx (:message/data msg)))
      :message/kind "reply"
      :message/status "ok")
    (catch Exception e
      (p/inc (op-collector ctx op :dwanascie-api/failed-server-requests-total))
      (log/error e "Failed to handle incoming RPC message")
      (failed-msg msg (try
                        (s/validate ErrorData (ex-data e))
                        (catch Exception _
                          {:error/type    "handler-failed"
                           :error/message (.getMessage ^Exception e)}))))))

(defn- wrap-handler [{:keys [in-exchange out-exchange schema] :as ctx} op handler]
  (fn [ch {:keys [delivery-tag headers] :as meta} ^bytes payload]
    (p/with-activity-counter (op-collector ctx op :dwanascie-api/active-server-requests-total)
      (p/with-duration (op-collector ctx op :dwanascie-api/server-request-duration-seconds)
        (p/inc (op-collector ctx op :dwanascie-api/server-requests-total))
        (try
          (validate-meta meta)
          (let [msg      (->> (deserialize payload)
                              (validate-incoming schema))
                last-op? (= op (last (:message/path msg)))]
            (log/debugf "Handler received API message: %s" (op-route->str msg))
            (log/tracef "Handler received API message: %s" (pr-str msg))
            (tracing/with-span (span-map msg headers op last-op?)
              (tracing/log {"received" (util/get-timestamp)})
              (if last-op?
                (if (= (:message/kind msg) "publish")
                  (try
                    (validate-in-data schema op msg)
                    (handler ctx (:message/data msg))
                    (catch Exception e
                      (p/inc (op-collector ctx op :dwanascie-api/failed-server-requests-total))
                      (log/error e "Failed to handle incoming publish message")))
                  (let [out         (reply-msg ctx op msg handler)
                        routing-key (reply-routing-key msg op)
                        meta        (default-meta true)]
                    (amqp-send ch out-exchange routing-key out meta)))
                (try
                  (validate-in-data schema op msg)
                  (let [data        (validate
                                      (out-schema schema op)
                                      (handler ctx (:message/data msg))
                                      (str "invalid output for op " op))
                        out         (assoc msg
                                      :message/data data)
                        next-op     (next-op (:message/path msg) op)
                        routing-key (if (:api/explicit-version msg)
                                      (str next-op "." (:api/explicit-version msg))
                                      next-op)
                        meta        (default-meta true)]
                    (amqp-send ch in-exchange routing-key out meta))
                  (catch Exception e
                    (p/inc (op-collector ctx op :dwanascie-api/failed-server-requests-total))
                    (log/errorf e "Failed to handle incoming message, next op canceled. op: %s path: %s" op (:message/path msg)))))))
          (catch Exception e
            (p/inc (op-collector ctx op :dwanascie-api/failed-server-requests-total))
            (log/warn e "Failed to handle incoming message"))
          (finally
            ;; TODO ack on error?
            (lb/ack ch delivery-tag)))))))

(defn- start-thread [fn]
  (let [t (Thread. ^Runnable fn)]
    (.start t)
    t))

(defn- start-consumer [{:keys [connection] :as ctx} q op consumer-count handler]
  (log/infof "Starting %d consumers for op %s..." consumer-count op)
  (let [channels (mapv (fn [idx]
                        (let [ch (lch/open connection)]
                          (lb/qos ch 1)
                          (lcf/select ch)
                          (start-thread #(lcons/subscribe ch q handler {:auto-ack     false
                                                                        :consumer-tag (str q "-" idx)}))
                          ch))
                      (range consumer-count))]
    {:channels       channels
     :operation      op
     :queue          q
     :tag            q
     :consumer-count consumer-count}))

(defn- name-server-op-queue [schema op explicit-version]
  (if explicit-version
    (str (name-operation-queue schema op) delimiter explicit-version)
    (name-operation-queue schema op)))

(def ^:private server-handler-queue-defaults
  {:exclusive false
   :auto-delete true
   :durable true
   :arguments {"x-max-priority" 10}})

(defn- start-server-threads [{:keys [schema api-channel in-exchange config] :as ctx} handlers explicit-version]
  (log/infof "Starting API server threads (%s)..." (:api/group schema))
  (let [consumers (->> handlers
                       (reduce (fn [server [op handler]]
                                 (let [{:keys [handler-fn handler-queue-config]} handler
                                       queue-name (name-server-op-queue schema op explicit-version)]
                                   (lq/declare api-channel queue-name (merge server-handler-queue-defaults handler-queue-config))
                                   (lq/bind api-channel queue-name in-exchange
                                            {:routing-key (if explicit-version
                                                            (str op "." explicit-version)
                                                            op)})
                                   (->> (wrap-handler ctx op handler-fn)
                                        (start-consumer ctx queue-name op (:amqp-consumer-count config))
                                        (assoc server op))))
                               {}))]
    (log/infof "Started API server threads (%s)" (:api/group schema))
    (assoc ctx :consumers consumers)))

(defn- stop-threads [{:keys [schema consumers] :as ctx}]
  (when consumers
    (log/infof "Stopping API server threads (%s)..." (:api/group schema))
    (doseq [{:keys [channels tag]} (vals consumers)
            [idx channel] (map-indexed vector channels)]
      (when channel
        (lb/cancel channel (str tag "-" idx))
        (rmq/close channel)))
    (log/infof "Stopped API server threads (%s)" (:api/group schema)))
  (dissoc ctx :consumers))

(defn- start-server [ctx {:keys [] :as config} api-schema handlers explicit-version]
  (-> ctx
      (connect api-schema config)
      (start-server-threads handlers explicit-version)))

(defn- stop-server [ctx]
  (-> ctx
      (stop-threads)
      (disconnect)))


;; Client

(defn pub->
  ([ctx op-path data]
   (pub-> ctx op-path data {}))

  ([{:keys [schema api-channel in-exchange config] :as ctx}
    op-path
    data
    {:keys [explicit-version priority]}]

   (let [op (first op-path)]
     (p/inc (op-collector ctx op :dwanascie-api/pub-requests-total))
     (try
       (tracing/with-span {:op-name (str "publish" delimiter op)
                           :tags    {"component" "dwanascie-api"
                                     "span.kind" "producer"}}
         (when-not (client-pub-path? schema op-path)
           (throw (ex-info "Invalid op-path for pub" {:op-path op-path})))
         (validate
           (in-schema schema op)
           data
           (format "invalid input data for op %s" op))
         (let [msg         (cond-> {:api/group        (:api/group schema)
                                    :api/version      (:api/version schema)
                                    :message/kind     "publish"
                                    :message/path     (vec op-path)
                                    :message/data     data
                                    :message/priority (or priority default-priority)}
                             explicit-version (assoc :api/explicit-version explicit-version))
               routing-key (if explicit-version
                             (str op "." explicit-version)
                             op)]
           (amqp-send
             api-channel in-exchange routing-key msg (default-meta true)
             (:amqp-ack-timeout-s config) (:amqp-max-retries config))))
       (catch Exception e
         (p/inc (op-collector ctx op :dwanascie-api/failed-pub-requests-total))
         (throw e))))))

(defn pub
  ([ctx op data]
   (pub-> ctx [op] data))
  ([ctx op data options]
   (pub-> ctx [op] data options)))

(defn- check-msg [msg]
  (case (:message/status msg)
    "ok" (:message/data msg)
    "failed" (throw (ex-info "RPC call failed" msg))))

(defn rpc->
  ([ctx op-path data]
   (rpc-> ctx op-path data {}))
  ([{:keys [schema api-channel in-exchange client-id dispatch timeouts] :as ctx}
    op-path
    data
    {:keys [priority timeout-ms handler ex-handler explicit-version] :as options}]
   (let [op (first op-path)]
     (p/inc (op-collector ctx op :dwanascie-api/rpc-requests-total))
     (try
       (tracing/with-span {:op-name (str "request" delimiter op)
                           :tags    {"component" "dwanascie-api"
                                     "span.kind" "client"}}
         (let [timeout (or timeout-ms 2000)]
           (when-not (rpc-path? schema op-path)
             (throw (ex-info "Invalid op-path for RPC" {:op-path op-path})))
           (validate
             (in-schema schema op)
             data
             (format "invalid input data for op %s" op))
           (let [request-id      (str (UUID/randomUUID))
                 response-chan   (async/chan 1 (map atom)) ;; "put!" a ref to (large) data
                 meta            (assoc (default-meta true)
                                   :expiration (str timeout))
                 msg             (cond-> {:api/group        (:api/group schema)
                                          :api/version      (:api/version schema)
                                          :message/kind     "request"
                                          :message/path     (vec op-path)
                                          :message/data     data
                                          :message/reply-to [client-id request-id]
                                          :message/priority (or priority default-priority)}
                                   explicit-version (assoc :api/explicit-version explicit-version))
                 routing-key     (if explicit-version
                                   (str op "." explicit-version)
                                   op)
                 request-headers (tracing/inject-map)]
             (swap! dispatch assoc request-id response-chan)
             (amqp-send api-channel in-exchange routing-key msg meta)
             (let [await-reply (fn []
                                 (p/with-activity-counter (op-collector ctx op :dwanascie-api/rpc-await-reply-total)
                                   (p/with-duration (op-collector ctx op :dwanascie-api/rpc-await-reply-duration-seconds)
                                     (try
                                       (let [[response-ref chan] (async/alts!! [response-chan (async/timeout timeout)])]
                                         (async/close! response-chan)
                                         (if (= chan response-chan)
                                           (let [response @response-ref]
                                             (reset! response-ref nil) ;; dump (large) data ref
                                             response)
                                           (do
                                             (p/inc (op-collector ctx op :dwanascie-api/rpc-request-timeouts-total))
                                             (swap! timeouts conj request-id)
                                             [(failed-msg msg
                                                          {:error/message (format "Timeout after %dms waiting for response" timeout)
                                                           :error/type    "timeout"})
                                              request-headers])))
                                       (catch InterruptedException e
                                         (log/debug e "RPC interrupted")
                                         (throw e))
                                       (finally
                                         (swap! dispatch dissoc request-id))))))]
               (if handler
                 (start-thread
                   #(let [[msg headers] (await-reply)]
                      (tracing/with-span
                        {:op-name      (str "async-response" delimiter op)
                         :follows-from headers
                         :tags         {"component" "dwanascie-api"
                                        "span.kind" "client"}}
                        (try
                          (->> (check-msg msg)
                               (handler ctx))
                          (catch Exception e
                            (tracing/log {"exception" (util/get-timestamp)})
                            (p/inc (op-collector ctx op :dwanascie-api/failed-rpc-requests-total))
                            (if ex-handler
                              (try
                                (tracing/with-span {:op-name "ex-handler"}
                                  (ex-handler ctx e))
                                (catch Exception e
                                  (log/error e "Failed to handle exception for failed incoming reply message handling")))
                              (log/error e "Failed to handle incoming reply message")))))))
                 (-> (await-reply)
                     first
                     (check-msg)))))))
       (catch Exception e
         (p/inc (op-collector ctx op :dwanascie-api/failed-rpc-requests-total))
         (throw e))))))

(defn rpc
  ([ctx op data]
   (rpc-> ctx [op] data))
  ([ctx op data options]
   (rpc-> ctx [op] data options)))

(defn- dispatch-fn [{:keys [schema] :as ctx} op dispatch timeouts]
  (fn [ch {:keys [delivery-tag headers] :as meta} ^bytes payload]
    (p/with-duration (op-collector ctx op :dwanascie-api/client-dispatch-duration-seconds)
      (try
        (validate-meta meta)
        (let [msg        (->> (deserialize payload)
                              (validate-reply schema op))
              request-id (second (:message/reply-to msg))
              chan       (get @dispatch request-id)]
          (log/debugf "Dispatch received API message: %s" (op-route->str msg))
          (log/tracef "Dispatch received API message: %s" (pr-str msg))
          (if chan
            (async/put! chan [msg (fix-headers headers)])
            (if (@timeouts request-id)
              (do
                (p/inc (op-collector ctx op :dwanascie-api/rpc-late-replies-total))
                (swap! timeouts disj request-id))
              (log/errorf "Unroutable message received: %s" request-id))))
        (catch Throwable e
          (log/error e "Dispatch impossible"))
        (finally
          (lb/ack ch delivery-tag))))))

(defn- start-client-consumer [ctx q op handler]
  (start-consumer ctx q op 1 handler))

(defn- start-client-threads [{:keys [schema api-channel] :as ctx}]
  (log/info "Starting API client threads...")
  (let [client-id (str (UUID/randomUUID))
        dispatch (atom {})
        timeouts (atom #{})
        consumers (reduce (fn [client operation-name]
                            (log/infof "Starting client consumer for %s..." operation-name)
                            (let [queue-name (str (name-operation-queue schema operation-name) delimiter client-id)
                                  ex (name-out-exchange schema)
                                  topic (str operation-name "." client-id ".*")]
                              (lq/declare api-channel queue-name {:exclusive true :auto-delete true :durable false :arguments {"x-max-priority" 10}})
                              (lq/bind api-channel queue-name ex {:routing-key topic})
                              (->> (dispatch-fn ctx operation-name dispatch timeouts)
                                   (start-client-consumer ctx queue-name operation-name)
                                   (assoc client operation-name))))
                          {}
                          (rpc-ops schema))]
    (log/info "Started API client threads")
    (merge ctx
           {:client-id client-id
            :dispatch  dispatch
            :timeouts  timeouts
            :consumers consumers})))

(defn- spy-fn [{:keys [schema] :as ctx} op  handler]
  (fn [ch {:keys [delivery-tag headers] :as meta} ^bytes payload]
    (try
      (validate-meta meta)
      (let [msg (->> (deserialize payload)
                     (validate-reply schema op))]
        (log/debugf "(spy) Received API message: %s" (op-route->str msg))
        (log/tracef "(spy) Received API message: %s" (pr-str msg))
        (tracing/with-span {:op-name      (str "spy" delimiter op)
                            :follows-from (fix-headers headers)
                            :tags         {"component" "dwanascie-api"
                                           "span.kind" "consumer"}}
          (handler ctx (:message/data msg))))
      (catch Exception e
        (log/error e "Failed to handle incoming spy message"))
      (finally
        (lb/ack ch delivery-tag)))))

(defn- start-spy-threads [{:keys [schema api-channel client-id config] :as ctx} spy-handlers]
  (let [threads (reduce (fn [threads op]
                          (if-let [handler (get spy-handlers op)]
                            (let [q     (str (name-operation-queue schema op) delimiter "spy" delimiter client-id)
                                  ex    (name-out-exchange schema)
                                  topic (str op ".#")]
                              (lq/declare api-channel q {:exclusive true :auto-delete true :durable true :arguments {"x-max-priority" 10}})
                              (lq/bind api-channel q ex {:routing-key topic})
                              (->> (spy-fn ctx op handler)
                                   (start-consumer ctx q op (:amqp-consumer-count config))
                                   (assoc threads (str op "-spy"))))
                            threads))
                        {}
                        (rpc-ops schema))]
    (update ctx :consumers merge threads)))

(defn- pub-fn [{:keys [schema] :as ctx} op  handler]
  (fn [ch {:keys [delivery-tag headers] :as meta} ^bytes payload]
    (try
      (validate-meta meta)
      (let [msg (->> (deserialize payload)
                     (validate-server-pub schema op))]
        (log/debugf "(pub) Received API message: %s" (op-route->str msg))
        (log/tracef "(pub) Received API message: %s" (pr-str msg))
        (tracing/with-span {:op-name      (str "consume" delimiter op)
                            :follows-from (fix-headers headers)
                            :tags         {"component" "dwanascie-api"
                                           "span.kind" "consumer"}}
          (handler ctx (:message/data msg))))
      (catch Exception e
        (log/error e "Failed to handle incoming pub message"))
      (finally
        (lb/ack ch delivery-tag)))))

(defn- start-pub-threads [{:keys [schema api-channel client-id config] :as ctx} pub-handlers]
  (let [threads (reduce (fn [threads operation]
                          (if-let [handler (get pub-handlers operation)]
                            (let [queue-name (str (name-operation-queue schema operation) delimiter client-id)
                                  exchange-name (name-out-exchange schema)]
                              (lq/declare api-channel queue-name {:exclusive true :auto-delete true :durable true :arguments {"x-max-priority" 10}})
                              (lq/bind api-channel queue-name exchange-name {:routing-key operation})
                              (->> (pub-fn ctx operation handler)
                                   (start-consumer ctx queue-name operation (:amqp-consumer-count config))
                                   (assoc threads (str operation "-pub"))))
                            threads))
                        {}
                        (server-pub-ops schema))]
    (update ctx :consumers merge threads)))

(defn- start-client [ctx config api-schema pub-handlers spy-handler]
  (-> ctx
      (connect api-schema config)
      (start-client-threads)
      (start-pub-threads pub-handlers)
      (start-spy-threads spy-handler)))

(defn- stop-client [ctx]
  (-> ctx
      (stop-threads)
      (disconnect)))


;; Service Discovery

(defn- uri-parts [uri]
  (let [uri (URI. uri)
        [user password] (str/split (.getUserInfo uri) #":")]
    {:vhost    (let [path (.getPath uri)]
                 (if (= path "")
                   "/"
                   path))
     :user     user
     :password password}))

(defn- api-bindings [schema bindings]
  (filter #(= (:source %) (name-in-exchange schema))
          bindings))

(defn- add-op [ops routing-key]
  (let [[_ o v] (re-find #"(.*)(\.[0-9]+\.[0-9]+\.[0-9]+)"
                         routing-key)
        versions (if v [(subs v 1)] [])
        op (or o routing-key)]
    (update-in ops [op :explicit-versions] concat versions)))

(defn available-ops [{:keys [schema config] :as ctx}]
  (let [{:keys [user password vhost]} (uri-parts (:amqp-uri config))
        url (str (:amqp-api-url config) "/api/bindings" vhost)
        res @(http/get url (cond-> {:as :json}
                            user (assoc :basic-auth [user password])))]
    (if (= (:status res) 200)
      (->>      (:body res)
                (api-bindings schema)
                (map :routing_key)
                (reduce add-op {}))
      (throw (ex-info "Failed to get status from RabbitMQ management API" {:response res})))))


;; Components

(defrecord ApiServer [config api-schema handlers explicit-version]

  component/Lifecycle

  (start [this]
    (log/info "Starting API Server...")
    (let [server (start-server this config api-schema handlers explicit-version)]
      (log/info "Started API Server")
      server))

  (stop [this]
    (log/info "Stopping API Server...")
    (let [server (stop-server this)]
      (log/info "Stopped API Server")
      server)))

(def api-default-config
  {:amqp-uri                    "amqp://guest:guest@rabbitmq"
   :amqp-api-url                "http://rabbitmq:15672"
   :amqp-connection-name        "dwanascie-api"
   :amqp-connect-max-attempts   30
   :amqp-connect-retry-delay-ms 2000
   :amqp-consumer-count         1})

(s/defschema ApiConfig
  {:amqp-uri                    s/Str
   :amqp-api-uri                s/Str
   :amqp-connection-name        s/Str
   :amqp-connect-max-attempts   s/Num
   :amqp-connect-retry-delay-ms s/Num
   :amqp-consumer-count         s/Num})

(defn new-api-server [api-schema {:keys [explicit-version handlers handlers-default-config handlers-config-shape]}]
  (map->ApiServer {:config-shape     (merge
                                       ApiConfig
                                       handlers-config-shape)
                   :config           (merge
                                       api-default-config
                                       handlers-default-config)
                   :api-schema       api-schema
                   :handlers         handlers
                   :explicit-version explicit-version
                   :metrics [[:gauge :dwanascie-api/active-server-requests-total
                              {:description "Number of requests currently handled by server."
                               :labels      [:group :op]}]
                             [:counter :dwanascie-api/server-requests-total
                              {:description "Total number of requests handled by server."
                               :labels      [:group :op]}]
                             [:counter :dwanascie-api/failed-server-requests-total
                              {:description "Total number of requests handled by server with failure."
                               :labels      [:group :op]}]
                             [:histogram :dwanascie-api/server-request-duration-seconds
                              {:buckets     [0.05 0.1 0.2 0.5 1 5 10 60 120 300 3600]
                               :description "Duration in seconds for requests handled by server."
                               :labels      [:group :op]}]]}))

(defrecord ApiClient [config api-schema pub-handlers spy-handlers]

  component/Lifecycle

  (start [this]
    (log/info "Starting RabbitMQ API Client...")
    (let [client (start-client this config api-schema pub-handlers spy-handlers)]
      (log/info "Started RabbitMQ API Client")
      client))

  (stop [this]
    (log/info "Stopping RabbitMQ API Client...")
    (let [client (stop-client this)]
      (log/info "Stopped RabbitMQ API Client")
      client)))

(defn new-api-client [api-schema {:keys [pub-handlers spy-handlers handlers-default-config handlers-config-shape]}]
  (map->ApiClient {:config-shape (merge
                                   ApiConfig
                                   handlers-config-shape)
                   :config       (merge
                                   api-default-config
                                   handlers-default-config)
                   :api-schema   api-schema
                   :spy-handlers spy-handlers
                   :pub-handlers pub-handlers
                   :metrics [[:counter :dwanascie-api/rpc-requests-total
                              {:description "Total number of rpc requests."
                               :labels      [:group :op]}]
                             [:counter :dwanascie-api/failed-rpc-requests-total
                              {:description "Total number of failed rpc requests."
                               :labels      [:group :op]}]
                             [:counter :dwanascie-api/rpc-request-timeouts-total
                              {:description "Total number of rpc request timeouts."
                               :labels      [:group :op]}]
                             [:counter :dwanascie-api/rpc-late-replys-total
                              {:description "Total number of replys received after timeout."
                               :labels      [:group :op]}]
                             [:gauge :dwanascie-api/rpc-await-reply-total
                              {:description "Number of rpc requests waiting for reply."
                               :labels      [:group :op]}]
                             [:histogram :dwanascie-api/rpc-await-reply-duration-seconds
                              {:buckets     [0.05 0.1 0.2 0.5 1 5 10 60 120 300 3600]
                               :description "Duration in seconds waiting for rpc reply."
                               :labels      [:group :op]}]
                             [:histogram :dwanascie-api/client-dispatch-duration-seconds
                              {:buckets     [0.05 0.1 0.15 0.2 0.25 0.3 0.4 0.5 0.75 1 2 5]
                               :description "Duration in seconds to dispatch received message."
                               :labels      [:group :op]}]
                             [:counter :dwanascie-api/pub-requests-total
                              {:description "Total number of publish requests."
                               :labels      [:group :op]}]
                             [:counter :dwanascie-api/failed-pub-requests-total
                              {:description "Total number of failed publish requests."
                               :labels      [:group :op]}]]}))
