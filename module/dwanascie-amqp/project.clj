(defproject dwanascie/dwanascie-amqp "0.29.1"
  :description "12-factor microservice framework - AMQP client component"
  :plugins [[lein-parent "0.3.8"]]
  :parent-project {:path "../../project.clj"
                   :inherit [:managed-dependencies :repositories :manifest :url :license :global-vars]}
  :dependencies [;; Clojure
                 [org.clojure/core.async "1.3.618"]
                 ;; dwanascie
                 [dwanascie/dwanascie-core]
                 ;; deps
                 [cheshire "5.10.0" :exclusions [com.fasterxml.jackson.core/jackson-core]] ;; covered by dwanascie core
                 [com.cognitect/transit-clj "1.0.324"]
                 [com.novemberain/langohr "5.2.0" :exclusions [clj-http]]
                 [clj-http "3.12.2"]])
