(ns dwanascie.t-api
  (:require [clojure.test :refer :all]
            [dwanascie.api :as api]
            [schema.core :as s]
            [dwanascie.system :as system]
            [clojure.core.async :as async]))

(def test-api
  {:api/group      "test"
   :api/version    "1"
   :api/operations {"inc"           {:in  {:num s/Num}
                                     :out {:num s/Num}}
                    "dup"           {:in  {:num s/Num}
                                     :out {:elements [s/Num]}}
                    "log"     {:in {:elements [s/Num]}}
                    "laber"         {:out {:msg s/Str}}
                    "err"           {:in  {:msg s/Str}
                                     :out {:msg s/Str}}
                    "unimplemented" {:in  {:msg s/Str}
                                     :out {:msg s/Str}}}})

(def server-handler
  {"inc" (fn [c m]
           (async/alts!! [(async/timeout 1000)])
           (println "inc: " m)
           (api/server-pub c "laber" {:msg "Doing nice inc!"})
           {:num (inc (:num m))})
   "dup" (fn [c m]
           (println "dup: " m)
           (api/server-pub c "laber" {:msg "Doing nice dup!"})
           {:elements (mapv (fn [_] (:num m)) (range (rand-int 10)))})
   "log" (fn [c m]
           (println "log: " m)
           (api/server-pub c "laber" {:msg "Received nice elements!"}))
   "err" (fn [c m]
           (println "err: " m)
           (api/server-pub c "laber" {:msg "I like failure sometimes!"})
           (throw (ex-info "allways fail!" {:error/type    "bla"
                                            :error/message "blub"})))})

(def definition
  {:service/type              "api-test"
   :service/name              "api-test"
   :service/version           "1.1"
   :service/components        {:server  (api/new-api-server
                                          test-api
                                          {:handlers server-handler})
                               :client  (api/new-api-client
                                          test-api
                                          {:pub-handlers {"laber" (fn [c m] (println "server event: " m)
                                                                    )}
                                           :spy-handlers {"inc" (fn [c m] (println "spy: " m)
                                                                  )}})}
   :service/metrics           []
   :service.logging/overrides {"com.zaxxer"       :error
                               "io.jaegertracing" :info}})

(defonce state (atom {}))

(defn start []
  (reset! state (system/start definition
                              {:shared {:amqp-uri  "amqp://guest:guest@localhost"
                                        :log-lever "trace"}}
                              "")))

(defn stop []
  (reset! state (system/stop @state)))


(deftest api-schema

  (is (= (s/validate api/ApiSchema test-api)
         test-api))

  )

(deftest test-operations-with-callback-queue
  ;; should return all queues with a callback channel
  (is (= (vec (api/get-rpc-ops (:api/operations test-api)))
         [["inc" {:in {:num Number}, :out {:num Number}}]
          ["dup" {:in {:num Number}, :out {:elements [Number]}}]
          ["err" {:in {:msg String}, :out {:msg String}}]
          ["unimplemented" {:in {:msg String}, :out {:msg String}}]])))