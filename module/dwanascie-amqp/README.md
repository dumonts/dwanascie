# Dwanascie AMQP

## Overview & usage

### RPC

![](https://www.rabbitmq.com/img/tutorials/python-six.png)

RPC calls a remote procedure and returns the response on a queue opened by the client. 
The request first has to be sent to the server
listening on a named queue. There is one for each operation registered with the server.

Client callback queues are short lived by default and have a unique identifier.
These callback queues are thrown away when the client
disconnects.

```clojure
;; Defining an API
(def operations {"calculate-fib" {:in {:digits s/Num}
                                  :out {:answer s/Num}}})

(def myApi
  {:api/group      "myApi"
   :api/version    "v13.1.2" ;; use the service version
   :api/operations operations})

;; Declaring the server
(api/new-api-server myApi
  {:handlers
    {"calculate-fib" 
     {:handler-fn #(calculation/calculate-fib %1 %2) ;; the remote procedure
      :handler-queue-config {;; this key is optional. Defaults are as follows
                             ;; used by only one connection and the queue will be deleted when that connection closes
                             :exclusive   false
                             ;; queue that has had at least one consumer is deleted when last consumer unsubscribes
                             :auto-delete true
                             ;; the queue will survive a broker restart
                             :durable     true
                             ;; broker-specific features such as message TTL, queue length limit, etc
                             :arguments   {"x-max-priority" 10}}}}})

;; Declaring the client
(api/new-api-client myApi {})

;; Calling the function
(api/rpc myApi "calculate-fib"
         {:digits 5} ;; payload
         {:handler    (handler-fn) ;; what to to with the response
          :ex-handler (handler-ex) ;; what to do in case of an exception
          :timeout-ms (get-in config [:timeouts :calculate-fib])})
```

This would result in something like this

```text
Server queue:
api/myApi/v13.1.2/calculate-fib

Client callback queue:
api/myApi/v13.1.2/calculate-fib/220fbfe9-fc1b-4f4c-9c8a-722ea341bbd5
```

### Publish

A publish is meant to be a one way send off for a message. 
Use pub for payloads you want to dispatch and forget. 
No callback queue is created during the initial start up of neither the server nor the client.

```clojure
;; Difference in declaring a publish vs rpc operation
1. (def operations {"log-message" {:in {:msg s/Str}}})

2. (def operations {"log-message-and-return-it" {:in {:msg s/Str}
                                                 :out {:msg s/Str}}})
```
In fact, they are used in the exact same way with the only difference being the handlers api schema.
A server function without an `:out` definition does not spawn a client thread and therefore qualifies as a publish. 
Example one in the listing above will NOT spawn a callback.
Example two will. 

```clojure
;; Declaring the server
(api/new-api-server myApiServer
                    {:handlers "log-message" #(calculation/calculate-fib %1 %2)})

;; Declaring the client
(api/new-api-client myApiServer {})

;; Calling the function
(api/pub myApi "log-message" payload)
```

Hence, this would result in something like this

```text
Server Queue
api/myApi/v3.0.2/log-message
```