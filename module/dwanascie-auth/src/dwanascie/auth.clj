(ns dwanascie.auth
  (:require [clojure.tools.logging :as log]
            [com.stuartsierra.component :as component]
            [jsonista.core :as json]
            [org.httpkit.client :as http]
            [org.httpkit.sni-client :as sni-client]
            [schema.core :as s])
  (:import com.nimbusds.jose.JWSAlgorithm
           com.nimbusds.jwt.JWTClaimsSet
           java.net.URL
           (com.nimbusds.jose.jwk.source JWKSource
                                         RemoteJWKSet)
           (com.nimbusds.jose.proc JWSKeySelector
                                   JWSVerificationKeySelector)
           (com.nimbusds.jwt.proc BadJWTException
                                  ConfigurableJWTProcessor
                                  DefaultJWTProcessor)))

;; ring.util.response ---
;; https://github.com/ring-clojure/ring/blob/1.9.0/ring-core/src/ring/util/response.clj
(defn response
  "Returns a skeletal Ring response with the given body, status of 200, and no
  headers."
  [body]
  {:status  200
   :headers {}
   :body    body})

(defn status
  "Returns an updated Ring response with the given status."
  ([status]
   {:status  status
    :headers {}
    :body    nil})
  ([resp status]
   (assoc resp :status status)))

(defn header
  "Returns an updated Ring response with the specified header added."
  [resp name value]
  (assoc-in resp [:headers name] (str value)))

(defn content-type
  "Returns an updated Ring response with the a Content-Type header corresponding
  to the given content-type."
  [resp content-type]
  (header resp "Content-Type" content-type))

;; ---

(defn keycloak-js-url [auth]
  (str (get-in auth [:config :auth-server-url])
       "/js/keycloak.js"))

(defn keycloak-json-handler [_ auth]
  ;; https://github.com/weavejester/ring-json-response/blob/master/src/ring/util/json_response.clj
  (-> (get auth :config)
      json/write-value-as-string
      response
      (content-type "application/json")))

(defn extract-claims [authenticator token]
  (try
    (-> (:jwt-processor authenticator)
        ^JWTClaimsSet (.process token nil)
        ^Map (.getClaims)
        (into {}))
    (catch BadJWTException e
      (log/warnf "Bad JWT: %s" (.getMessage e)))
    (catch Exception e
      (log/warn e "Failed to get token claims"))))

(defn get-bearer-token [req]
  (when-let [^String header (get-in req [:headers "authorization"])]
    (when (.startsWith header "Bearer ")
      (subs header 7))))

(defn wrap-auth [handler auth]
  (fn [req]
    (if-let [claims (->> (get-bearer-token req)
                         (extract-claims auth))]
      (handler (assoc req :user (if-let [user-fn (get-in auth [:user-fn])]
                                  (user-fn req claims)
                                  claims)))
      (-> (response "Access Denied")
          (status 403)))))

(defn- check-connection [url]
  (binding [http/*default-client* sni-client/default-client]
    (let [{:keys [status] :as response} @(http/get url {:insecure? false})
          expected-status #{200 301}]
      (when-not (expected-status status)
        (throw
          (ex-info
            (format "Failed to get certs from auth server (%s): %s" url (:body response))
            response))))))

(defn- init [auth-server-internal-url realm]
  (let [url (str auth-server-internal-url "/realms/" realm "/protocol/openid-connect/certs")
        jwt-processor ^ConfigurableJWTProcessor (DefaultJWTProcessor.)
        key-source    ^JWKSource (RemoteJWKSet. (URL. url))
        alg ^JWSAlgorithm JWSAlgorithm/RS256
        key-selector ^JWSKeySelector (JWSVerificationKeySelector. alg key-source)]
    (log/infof "OpenID Connect Server URL: %s" (pr-str url))
    (check-connection url)
    (.setJWSKeySelector jwt-processor key-selector)
    jwt-processor))

(s/defschema AuthConfig
  {:auth-server-internal-url s/Str
   :auth-server-url          s/Str
   :realm                    s/Str
   :resource                 s/Str
   :ssl-required             s/Str
   :public-client            s/Bool
   :confidential-port        s/Num})

(def default-config
  {:auth-server-internal-url "localhost"
   :auth-server-url          "localhost"
   :realm                    "default"
   :resource                 "default"
   :ssl-required             "external"
   :public-client            true
   :confidential-port        0})

(defrecord OpenIDConnectAuth [config options jwt-processor user-fn]

  component/Lifecycle

  (start [this]
    (log/info "Starting OpenID Connect Auth...")
    (let [{:keys [auth-server-internal-url realm]} config
          new-jwt-processor (init auth-server-internal-url realm)]
      (log/info "Started OpenID Connect Auth")
      (assoc this :jwt-processor new-jwt-processor)))

  (stop [this]
    (log/info "Stopped OpenID Connect Auth")
    (assoc this :jwt-processor nil)))

(defn new-openid-connect-auth [user-fn]
  (map->OpenIDConnectAuth {:user-fn      user-fn
                           :config-shape AuthConfig
                           :config       default-config}))