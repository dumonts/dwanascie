(defproject dwanascie/dwanascie-datomic "0.28.2"
  :description "12-factor microservice framework - datomic peer component"
  :plugins [[lein-parent "0.3.8"]]
  :parent-project {:path "../../project.clj"
                   :inherit [:managed-dependencies :repositories :manifest :url :license :global-vars]}
  :dependencies [;; dwanascie
                 [dwanascie/dwanascie-core]
                 ;; deps
                 [com.datomic/datomic-free "0.9.5697" :exclusions [[org.slf4j/log4j-over-slf4j]
                                                                   [org.slf4j/jul-to-slf4j]]]])
