(ns dwanascie.datomic-peer
  (:require [clojure.string :as str]
            [clojure.tools.logging :as log]
            [com.stuartsierra.component :as component]
            [datomic.api :as d]
            [schema.core :as s]))

(defn- connect [uri]
  (try
    (d/connect uri)
    (catch Exception e
      (log/error e "Failed to connect to Datomic")
      (throw e))))

(defn- db-uri [{:keys [datomic-base-uri] :as peer} db-name]
  (str/replace-first datomic-base-uri "*" db-name))

(defn get-conn [{:keys [conns] :as peer} db-name]
  (or (get @conns db-name)
      (let [uri (db-uri peer db-name)]
        (swap! conns assoc db-name (connect uri))
        (recur peer db-name))))

(defn get-db [peer db-name tx]
  (-> (get-conn peer db-name)
      (d/db)
      (d/as-of tx)))

(defn current-tx-t [peer kb-name]
  (-> (get-conn peer kb-name)
      d/db
      d/basis-t))

(defn current-tx-id [peer kb-name]
  (d/t->tx
    (current-tx-t peer kb-name)))

(defrecord DatomicPeer [config conns]

  component/Lifecycle

  (start [this]
    (log/info "Starting Datomic Peer...")
    (let [peer (-> this
                   (assoc :conns (atom {}))
                   (merge config))]
      (try
        (d/get-database-names (:datomic-base-uri config))
        (catch Exception e
          (log/fatal e "Connection to Datomic could not be established")
          (throw e)))
      (log/info "Started Datomic Peer")
      peer))

  (stop [this]
    (when conns
      (log/info "Stopping Datomic Peer...")
      (doseq [conn (vals @conns)]
        (when conn
          (d/release conn))))
    (log/info "Stopped Datomic Peer")
    (assoc this :conns nil)))

(def default-config
  {:datomic-base-uri "datomic:mem://*"})

(s/defschema DatomicPeerConfig
  {:datomic-base-uri s/Str})

(defn new-datomic-peer []
  (map->DatomicPeer {:config-shape DatomicPeerConfig
                     :config       default-config}))
