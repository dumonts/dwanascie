(defproject dwanascie/dwanascie-core "0.28.3"
  :description "12-factor microservice framework"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}
  :url "https://gitlab.com/dumonts/dwanascie"
  :managed-dependencies [[org.clojure/clojure "1.10.3"]
                         [dwanascie/dwanascie-core "0.28.3"]]
  :src-paths    ["src"]
  :global-vars  {*warn-on-reflection* true}
  :dependencies [;; Clojure
                 [org.clojure/clojure]
                 [org.clojure/tools.cli "1.0.206"]
                 [org.clojure/tools.logging "1.1.0"]

                 ;; Deps
                 [com.stuartsierra/component "1.0.0"]
                 [http-kit "2.5.3"]
                 [iapetos "0.1.8" :exclusions [io.prometheus/simpleclient]]
                 [io.forward/yaml "1.0.10"]
                 [io.jaegertracing/jaeger-client "1.6.0" :exclusions [[org.jetbrains.kotlin/kotlin-stdlib-common]
                                                                      [org.apache.tomcat.embed/tomcat-embed-core]]]
                 [io.prometheus/simpleclient_hotspot "0.11.0"] ;; Hotspot JVM metrics provider
                 [metosin/jsonista "0.3.3"]
                 [metosin/schema-tools "0.12.3"]
                 [org.apache.tomcat.embed/tomcat-embed-core "8.5.66"]
                 [prismatic/schema "1.1.12"]
                 [spootnik/unilog "0.7.27" :exclusions [com.fasterxml.jackson.core/jackson-core]]]
  :profiles {:test {:dependencies [[prismatic/schema-generators "0.1.3"]]}})
